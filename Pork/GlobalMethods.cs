﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Media;
using System.Runtime.Serialization.Formatters.Binary;

namespace Pork {

    /// <summary>
    /// Methods that should be globally reachable
    /// </summary>
    static class GlobalMethods {

        /// <summary>
        /// This is only used for the Music methods (PlayMusic/StopMusic/etc)
        /// </summary>
        private static SoundPlayer _musicPlayer = new SoundPlayer();

        /// <summary>
        /// Simply check for a collision against an ISolid object in the given position parameter
        /// </summary>
        /// <param name="positionToCheck">The position to check for a ISolid object in</param>
        /// <returns>True if an object was found, false otherwise</returns>
        public static bool CheckForSolidCollisions(Point positionToCheck) {
            foreach (var solidObj in GameVariables.GameObjectsList.Where(item => item is ISolid && item.Enabled)) {
                if ((solidObj as ISolid).SolidActivated &&
                    solidObj.Position.Equals(positionToCheck)) {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Simply check for any object in the given position parameter
        /// </summary>
        /// <param name="positionToCheck">The position to check for a gameobject in</param>
        /// <returns>The object that's in that location</returns>
        public static GameObject CheckForGameObjects(Point positionToCheck) {
            return GameVariables.GameObjectsList.FirstOrDefault(obj => obj.Enabled && obj.Position.Equals(positionToCheck));
        }

        /// <summary>
        /// Clear any lists stored in GameVariables and then clear the console window
        /// </summary>
        public static void ClearGameBuffer() {
            GameVariables.GameObjectsList.Clear();
            Console.Clear();
        }

        /// <summary>
        /// Draw the player HUD on the screen
        /// </summary>
        /// <param name="player">The player object so we can get the avatar and information</param>
        public static void DrawPlayerHud(IPlayer player) {

            if (player.AvatarImage == null) {
                throw new Exception("Player needs an avatar!");
            }
            if (GameVariables.CurrentLevel == null) {
                throw new Exception("Level cannot be null before drawing HUD!");
            }

            #region check avatar requirements
            if (player.AvatarImage.Length != GameVariables.PlayerAvatarHeightRequirement) {
                throw new Exception(String.Format("Player avatar must be exactly {0} lines in height.", GameVariables.PlayerAvatarHeightRequirement));
            }

            foreach (string line in player.AvatarImage) {
                if (line.Length != GameVariables.PlayerAvatarWidthRequirement) {
                    throw new Exception(String.Format("Line {0} needs to be exactly {1} in width.", line, GameVariables.PlayerAvatarWidthRequirement));
                }
            }
            #endregion

            #region Set instruction key lengths
            int width = (GameVariables.CurrentLevel.LevelWidth < GameVariables.MinimumHudWidth ? GameVariables.MinimumHudWidth : GameVariables.CurrentLevel.LevelWidth);
            int maxCharsForInstructions = 3; // The max number of characters to print in the HUD for keys (i.e. Escape = Esc)

            string gameplayMenuKey = Enum.GetName(typeof(ConsoleKey), Controls.GameplayMenuKey);
            if (gameplayMenuKey.Length > maxCharsForInstructions) {
                gameplayMenuKey = gameplayMenuKey.Substring(0, maxCharsForInstructions);
            }

            string inventoryMenuKey = Enum.GetName(typeof(ConsoleKey), Controls.InventoryKey);
            if (inventoryMenuKey.Length > maxCharsForInstructions) {
                inventoryMenuKey = inventoryMenuKey.Substring(0, maxCharsForInstructions);
            }

            string actionKey = Enum.GetName(typeof(ConsoleKey), Controls.ActionKey);
            if (actionKey.Length > maxCharsForInstructions) {
                actionKey = actionKey.Substring(0, maxCharsForInstructions);
            }

            #endregion

            // String for the "keys" instructions
            string instructionsLine = String.Format("[{0}] = Gameplay Menu | [{1}] = Inventory | [{2}] = Action ", gameplayMenuKey, inventoryMenuKey, actionKey);
            // Stats on the player
            string statsLine = String.Format(" Health: {0,-3} | {1,-11} | ${2,-6} | {3,-10} ", player.Health, GameVariables.CurrentLevel.LevelName,
                                                player.Currency, player.CurrentWeapon.EquipmentName);

            #region center the instructions and stats
            int contentWidth = width - GameVariables.PlayerAvatarWidthRequirement - GameVariables.HudBufferSpace;

            for (int i = 0, j = 0; i < contentWidth - instructionsLine.Length; j++) {
                if (j % 2 == 0) {
                    instructionsLine = " " + instructionsLine;
                } else {
                    instructionsLine += " ";
                }
            }

            for (int i = 0, j = 0; i < contentWidth - statsLine.Length; j++) {
                if (j % 2 == 0) {
                    statsLine = " " + statsLine;
                } else {
                    statsLine += " ";
                }
            }

            #endregion

            #region draw hud
            string hud = "";
            hud += new string('#', width);
            hud += String.Format("#{0}#{1}#", (player.AvatarImage.Length > 0? player.AvatarImage[0] : new string(' ', GameVariables.PlayerAvatarWidthRequirement)) , instructionsLine);
            hud += String.Format("#{0}#{1}#", (player.AvatarImage.Length > 1? player.AvatarImage[1] : new string(' ', GameVariables.PlayerAvatarWidthRequirement)), new string('-', contentWidth));
            hud += String.Format("#{0}#{1}#", (player.AvatarImage.Length > 2? player.AvatarImage[2] : new string(' ', GameVariables.PlayerAvatarWidthRequirement)), statsLine);
            hud += String.Format("#{0}#{1}#", (player.AvatarImage.Length > 3? player.AvatarImage[3] : new string(' ', GameVariables.PlayerAvatarWidthRequirement)), new string(' ', contentWidth));
            hud += new string('#', width);
            #endregion

            // Make sure the cursor is in the right place to draw the HUD
            Console.SetCursorPosition(GameVariables.HudPosition.X, GameVariables.HudPosition.Y);
            hud.WriteToConsole(GameVariables.HudColor);
        }

        /// <summary>
        /// Draw a dialogue box with the text above the HUD. This handles pagination 
        /// using Controls.ActionKey (WILL pause the game while the player reads)
        /// </summary>
        /// <param name="text">The text to print (in array for word wrap to GameVariables.DialogueWindowWidth)</param>
        /// <param name="dialogueTitle">The title to display in the box (default is blank)</param>
        public static void DrawDialogueBox(string[] text, string dialogueTitle = "") {
            // Get the box xhape for the dialogue
            Box dialogueBox = new Box(
                GameVariables.MinimumHudWidth / 2 - GameVariables.DialogueWindowWidth / 2,
                GameVariables.MinimumHudWidth / 2 + GameVariables.DialogueWindowWidth / 2,
                GameVariables.CurrentLevel.LevelHeight - GameVariables.DialogueWindowHeight - 1, // 1 just for an extra space between it and the bottom of the level
                GameVariables.CurrentLevel.LevelHeight - 1);

            // Start with drawing the dialogue box first
            DrawMainDialogueBox(dialogueBox, dialogueTitle);

            int currentRow = 0; // Which row we're on when writing the text (resets each page)
            int maxLinesAtATime = GameVariables.DialogueWindowHeight - 2; // Got to "word wrap" after we have no more space
            Console.SetCursorPosition(dialogueBox.Left, dialogueBox.Top + 1); // Start at the beginning of the inside of the box

            // Print all the text provided in text[]
            for (int i = 0; i < text.Length; i++) {
                string line = text[i];

                // As long as we're still not exceeding the dialogue box, write a new line in the array text[]
                if (currentRow++ < maxLinesAtATime) {
                    // Center our text for this item in text[] and then print it out
                    int textStartPosition = (dialogueBox.Left + dialogueBox.Right) / 2 - line.Length / 2;
                    Console.SetCursorPosition(textStartPosition, Console.CursorTop);
                    line.WriteToConsole(GameVariables.DialogueTextColor);
                    // Move down one line
                    Console.SetCursorPosition(dialogueBox.Left, Console.CursorTop + 1); 
                } else {
                    // Reset the rows and clear out the text so we can go to the next page
                    currentRow = 0;
                    // But first, wait for the palyer to click the ActionKey to proceed to the next page
                    while (Console.ReadKey(true).Key != Controls.ActionKey) ;
                    DrawMainDialogueBox(dialogueBox, dialogueTitle);
                    Console.SetCursorPosition(dialogueBox.Left, dialogueBox.Top + 1);
                    i--; //retry the same text line (Since it was out of the dialog box limit)
                }
            }

            // Some text was printed out, wait for player input
            if (currentRow != 0) {
                while (Console.ReadKey(true).Key != Controls.ActionKey) ;
            }

            //Erase the dialogue box so it doesn't stick around forever
            for (int i = 0; i < GameVariables.DialogueWindowHeight; i++) {
                Console.SetCursorPosition(dialogueBox.Left, dialogueBox.Top + i);
                Console.Write(new string(' ', GameVariables.DialogueWindowWidth));
            }

            // In case an object was hiding under the screen, refresh the whole draw screen
            GameVariables.RefreshScreen = true;
        }

        /// <summary>
        /// Draw the main dialogue box (no text included, just the title)
        /// </summary>
        /// <param name="dialogueBox">The box object for the dialogue</param>
        /// <param name="dialogueTitle">The title of the box (i.e. an NPC's name)</param>
        public static void DrawMainDialogueBox(Box dialogueBox, string dialogueTitle) {
            // Set the size of the box vars
            var width = dialogueBox.Right - dialogueBox.Left;
            var height = dialogueBox.Bottom - dialogueBox.Top;
            
            //Draw the top border with '+' chars, surrounding the title with some space
            Console.SetCursorPosition(dialogueBox.Left, dialogueBox.Top);
            (new string('+', width)).WriteToConsole(GameVariables.DialogueBorderColor);
            Console.SetCursorPosition(dialogueBox.Left + 2, Console.CursorTop);
            (" " + dialogueTitle + " ").WriteToConsole(GameVariables.DialogueTitleColor);

            // For the left/right edges
            for (int i = 0; i < height - 2; i++) {
                Console.SetCursorPosition(dialogueBox.Left, Console.CursorTop + 1);
                "+".WriteToConsole(GameVariables.DialogueBorderColor);
                Console.Write(new string(' ', width - 2));
                "+".WriteToConsole(GameVariables.DialogueBorderColor);
            }
            
            // Finally, draw the bottom border along with the instructions to go to the next screen
            Console.SetCursorPosition(dialogueBox.Left, Console.CursorTop + 1);
            (new string('+', width)).WriteToConsole(GameVariables.DialogueBorderColor);
            string instructionNote = String.Format(" [{0}] Next ", Controls.ActionKey );
            Console.SetCursorPosition(dialogueBox.Left + (width - instructionNote.Length - 1), Console.CursorTop);
            instructionNote.WriteToConsole(GameVariables.DialogueTitleColor);
        }

        /// <summary>
        /// Determine whether an object should be drawn or not based on the chapter given
        /// </summary>
        /// <param name="questObject">The object to check</param>
        /// <param name="showAtChapter">Whether it should show up at this chapter</param>
        /// <param name="chapter">Which chapter it should be seen/hidden on</param>
        /// <returns></returns>
        public static bool ContinueUpdateOrDraw(GameObject questObject, bool showAtChapter, int chapter) {
            // show only before the given chapter. Delete afterwards
            if (!showAtChapter && GameVariables.CurrentChapter >= chapter) {
                questObject.WaitingRemoval = true;
            }
            // Not ready to show yet
            else if (showAtChapter && GameVariables.CurrentChapter < chapter) {
                return false;
            }

            // Assume true otherwise
            return true;
        }

        /// <summary>
        /// Play the music if music is enabled
        /// </summary>
        /// <param name="musicName">The name of the music file (must be a wav file)</param>
        public static void PlayMusic(string musicName) {
            // Only play music if the setting is enabled
            if (!GameVariables.MusicEnabled) {
                return;
            }

            // Add .wav as the extension if it wasn't included
            if (!musicName.EndsWith(".wav")) {
                musicName += ".wav";
            }

            // Make sure the music file exists in the sounds directory first!
            if (File.Exists(GameVariables.SoundsDirectory + musicName)) {
                _musicPlayer.Stop(); //stop it to start it over
                _musicPlayer.SoundLocation = GameVariables.SoundsDirectory + musicName;
                _musicPlayer.PlayLooping();
            }
            
        }

        /// <summary>
        /// Play some kind of sound if sounds are enabled
        /// </summary>
        /// <param name="soundName">The name of the sound file (must be a wav file)</param>
        public static void PlaySound(string soundName) {
            // Only play sounds if the setting is enabled
            if (!GameVariables.SoundEnabled) {
                return;
            }

            // Add .wav as the exteension if it wasn't included
            if (!soundName.EndsWith(".wav")) {
                soundName += ".wav";
            }

            // Make sure the sound file exists in the sounds directory first!
            if (File.Exists(GameVariables.SoundsDirectory + soundName)) {
                // only needs to exist as long as we play the sound
                SoundPlayer sp = new SoundPlayer(GameVariables.SoundsDirectory + soundName);
                sp.Play();
            }            
        }

        /// <summary>
        /// Stop the music currently playing
        /// </summary>
        public static void StopMusic() {
            _musicPlayer.Stop();
        }

        /// <summary>
        /// Load the game configurations file located at GameVariables.SaveConfigurationLocation
        /// </summary>
        public static void LoadGameConfiguration() {
            // Make sure the directory exists as well as the file
            if (Directory.Exists(GameVariables.SaveConfigurationLocation) &&
                File.Exists(GameVariables.SaveConfigurationLocation + "config.txt")) {
                // Get all the file information
                var lines = File.ReadAllLines(GameVariables.SaveConfigurationLocation + "config.txt");

                try {
                    string setting;
                    setting = GetConfigurationSetting(lines, "SOUND");
                    GameVariables.SoundEnabled = Boolean.Parse(setting);
                    setting = GetConfigurationSetting(lines, "MUSIC");
                    GameVariables.MusicEnabled = Boolean.Parse(setting);
                    setting = GetConfigurationSetting(lines, "COLORS");
                    GameVariables.ColorsEnabled = Boolean.Parse(setting);
                    setting = GetConfigurationSetting(lines, "SAVEGAMELOCATION");
                    GameVariables.SaveGameLocation = setting;
                    setting = GetConfigurationSetting(lines, "FONTSIZE");
                    GameVariables.CurrentFont.Index = UInt32.Parse(setting);
                    ConsoleHelper.SetConsoleFont(UInt32.Parse(setting));
                } catch (Exception error) {
                    "An error occured. Likely the configuration file is not formatted correctly.\n".WriteToConsole(ItemColors.BadMessage);
                    error.Message.WriteToConsole(ItemColors.BadMessage);
                    Console.ReadLine();
                }
            } else {
                // File doesn't exist, create it
                SaveGameConfiguration();
            }
        }

        [Serializable]
        private struct GameDataStruct {
            public uint PlayerAF;
            public string PlayerName;
            public string[] PlayerAvatar;
            public int PlayerHealth;
            public int PlayerMaxHealth;
        }

        /// <summary>
        /// Loads the game
        /// </summary>
        /// <param name="slot">The game slot to load from</param>
        public static void LoadGameData(int slot) {
            //TODO: MAKE WORK
            return;

            string file = string.Format("{0}\\Game{1}.dat", GameVariables.SaveGameLocation, slot);
            if (!File.Exists(file)) {
                return;
            }

            using (FileStream fs = File.Open(file, FileMode.Open)) {
                GameDataStruct newObject = (GameDataStruct)(new BinaryFormatter().Deserialize(fs));

            }
        }

        /// <summary>
        /// Saves the current game
        /// </summary>
        /// <param name="slot">The slot to save the game to</param>
        public static void SaveGameData(int slot) {
            using (FileStream fs = File.Open(string.Format("{0}\\Game{1}.dat", GameVariables.SaveGameLocation, slot), FileMode.OpenOrCreate)) {
                List<Player> ps = new List<Player>();
                GameVariables.GameObjectsList.Where(obj => obj is Player).ToList().ForEach(obj => ps.Add(obj as Player));

                //TODO save game data!
                GameDataStruct thisObject = new GameDataStruct {

                };
                new BinaryFormatter().Serialize(fs, thisObject);
            }
        }

        /// <summary>
        /// Save the configuration file in GameVariables.SaveConfigurationLocation
        /// </summary>
        public static void SaveGameConfiguration() {
            try {
                List<string> settings = new List<string>();
                settings.Add(string.Format("SOUND={0}", GameVariables.SoundEnabled));
                settings.Add(string.Format("MUSIC={0}", GameVariables.MusicEnabled));
                settings.Add(string.Format("COLORS={0}", GameVariables.ColorsEnabled));
                settings.Add(String.Format("SAVEGAMELOCATION={0}", GameVariables.SaveGameLocation));
                settings.Add(string.Format("FONTSIZE={0}", GameVariables.CurrentFont.Index));

                Directory.CreateDirectory(GameVariables.SaveConfigurationLocation);
                File.WriteAllLines(GameVariables.SaveConfigurationLocation + "config.txt", settings);
            } catch (Exception error) {
                "An error occured when trying to write the configuration file.\n".WriteToConsole(ItemColors.BadMessage);
                error.Message.WriteToConsole(ItemColors.BadMessage);
                Console.ReadLine();
            }
        }

        /// <summary>
        /// Get a specific setting loaded from the lines given (looks through them, splitting by '=')
        /// </summary>
        /// <param name="lines">The linse from the configuration file</param>
        /// <param name="configSettingLabel">The label we're searching for and wanting the setting for</param>
        /// <returns>The string value of the setting, if it exists</returns>
        public static string GetConfigurationSetting(string[] lines, string configSettingLabel) {
            string settingValue = "";

            try {
                settingValue = lines.FirstOrDefault(l => l.ToUpperInvariant().StartsWith(configSettingLabel.ToUpperInvariant())).Split('=')[1];
            } catch (Exception error) when (error is IOException || error is IndexOutOfRangeException || error is NullReferenceException) {
                String.Format("An expected configuration file doesn't exist or doesn't contain a value to the element '{0}'\n", configSettingLabel)
                    .WordWrap()
                    .WriteToConsole(ItemColors.BadMessage);
                Console.ReadLine();
            }

            return settingValue;
        }
    }
}
