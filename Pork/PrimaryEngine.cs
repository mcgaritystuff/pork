﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.IO;

namespace Pork {
    public sealed class PrimaryEngine {
        #region fields
        private const int OneSecond = 1000; //1000 milliseconds

        private static float _gameLoopInterval;
        private static bool _initialized = false;
        private static DateTime _engineTimer;
        private static MenuSystem _mainMenu;
        private static MenuSystem _gameplayMenu;
        private static Dictionary<Level, List<GameObject>> _levelCache; //Helps store information such as enemies, items, etc. rather than recreating the levels over again.

        private static string _initialDialogue = ("Welcome to the tutorial! To start things off, the Action " +
                                                 "Key is [$ACTION]. This is to talk to NPCs, pick up " +
                                                 "items, read signs, etc. You can move up, down, left, and " +
                                                 "right with [$UP1], [$DOWN1], [$LEFT1], and [$RIGHT1] " +
                                                 "respectively. Oh, and lastly, press [$JOURNAL] for the journal!").ReplaceKeywords();
        #endregion

        /// <summary>
        /// This will initialize the engine to run at a frame rate set by FrameRate.
        /// </summary>
        public static void Initialize() {
            if (_initialized) {
                throw new Exception("Engine already initialized!");
            }

            try {
                GameVariables.CurrentGameState = GameState.MainMenu;

                GameVariables.ColorsEnabled = true;
                GameVariables.MusicEnabled = true;
                GameVariables.SoundEnabled = true;

                _mainMenu = new MenuSystem(new MainMenu(), true);
                _gameplayMenu = new MenuSystem(new GameplayMenu(), false);

                _gameLoopInterval = OneSecond / GameVariables.FrameRate;
                _engineTimer = DateTime.Now;

                GameVariables.IncrementCurrentChapter += (() => GameVariables.CurrentChapter++);
                GameVariables.InitializeGameSettings += InitializeGame;
                GameVariables.InitializeTutorial += InitializeTutorial;

                ConsoleHelper.SetConsoleFont(GameVariables.CurrentFont.Index);
                GlobalMethods.LoadGameConfiguration();
                GlobalMethods.LoadGameData(0);//TODO

                GlobalMethods.PlayMusic(GameVariables.MenuMusicFile);

                _initialized = true;
            } catch (Exception error) {
                Console.WriteLine("Error: " + error);
            }
        }

        /// <summary>
        /// This is where the game loop begins. You *must* call Initialize() before Run()
        /// so that all the variables required are set.
        /// </summary>
        public static void Run() {
            try {
                if (!_initialized) {
                    throw new Exception("Engine not initialized!!");
                }

                //Game loop
                while (true) {
                    //play at requested FrameRate
                    if ((DateTime.Now - _engineTimer).TotalMilliseconds >= _gameLoopInterval) {
                        if (Console.KeyAvailable) {
                            GameVariables.CurrentKeyPressed = Console.ReadKey(true).Key;
                        }

                        _engineTimer = DateTime.Now;

                        if (GameVariables.CurrentKeyPressed == Controls.GameplayMenuKey) {
                            GameVariables.GamePaused = true;
                            GameVariables.CurrentGameState = GameState.GameplayMenu;
                        }

                        switch (GameVariables.CurrentGameState) {
                            case GameState.Gameplay:
                                RunGame();
                                break;
                            case GameState.GameplayMenu:
                                _gameplayMenu.RunMenu();
                                break;
                            case GameState.MainMenu:
                                _mainMenu.RunMenu();
                                break;
                        }

                        //set the key to a button we won't use for the next loop
                        GameVariables.CurrentKeyPressed = ConsoleKey.NoName;
                    }
                }
            } catch (Exception error) {
                Console.WriteLine(String.Format("Error!!! {0}", error.Message));
                Console.ReadLine();
            }
        }

        /// <summary>
        /// This is the loop to the main part of the game
        /// </summary>
        private static void RunGame() {
            if (GameVariables.CurrentKeyPressed == Controls.PauseGameKey ||
                GameVariables.CurrentKeyPressed == Controls.JournalKey && GameVariables.GamePaused) {
                // Pause the game when pausing or viewing the journal!
                GameVariables.GamePaused = !GameVariables.GamePaused;
                GameVariables.RefreshScreen = true;
                return;
            }

            //update and draw anything in the game
            if (!GameVariables.GamePaused) {
                RunUpdatesAndDraws();
            }

            //If we have to refresh the screen for some reason
            if (GameVariables.RefreshScreen) {
                Console.Clear();
                RunInitialDraws();
                GameVariables.RefreshScreen = false;
            }

            //check players first. Don't want to accidentally go to the next level first.
            if (GameVariables.GameOver) {
                GameOver();
                GameVariables.CurrentGameState = GameState.MainMenu;
                GlobalMethods.PlayMusic(GameVariables.MenuMusicFile);
                return;
            }

            // Lastly, if we're trying to go to a different level, do eeeet
            if (GameVariables.NextLevel != null) {
                try {
                    GoToNextLevel();
                    GameVariables.NextLevel = null;
                } catch (Exception error) {
                    throw error;
                }
            }
        }

        /// <summary>
        /// Initialize the GAME itself
        /// </summary>
        private static void InitializeGame() {
            try {
                _levelCache = new Dictionary<Level, List<GameObject>>();
                GameVariables.DungeonStack = new Stack<string>();
                GameVariables.GameObjectsList = new List<GameObject>();
                GameVariables.CurrentChapter = 0;
                GameVariables.CurrentLevel = null;
                GameVariables.GameOver = false;
                GameVariables.GamePaused = false;
                GameVariables.RefreshScreen = false;
                GameVariables.NextLevel = "START";

                GoToNextLevel();

                Console.CursorVisible = false;
                Console.Title = GameVariables.ProgramName;
                GameVariables.IncrementCurrentChapter();
                GlobalMethods.StopMusic();
            } catch (Exception error) {
                string.Format("Error occured! Note levels should be all placed in: {0}", GameVariables.LevelDirectory).WriteToConsole(ItemColors.BadMessage);
                "Also Note that \"START\" *IS* a required level!".WriteToConsole(ItemColors.BadMessage);
                string.Format("Error: {0}", error).WriteToConsole(ItemColors.BadMessage);
            }
        }

        /// <summary>
        /// Initialize the starter tutorial
        /// </summary>
        private static void InitializeTutorial() {
            try {
                _levelCache = new Dictionary<Level, List<GameObject>>();
                GameVariables.DungeonStack = new Stack<string>();
                GameVariables.GameObjectsList = new List<GameObject>();
                GameVariables.CurrentChapter = 0;
                GameVariables.CurrentLevel = null;
                GameVariables.GameOver = false;
                GameVariables.GamePaused = false;
                GameVariables.RefreshScreen = false;
                GameVariables.NextLevel = "Tutorial 1";

                GoToNextLevel();

                Console.CursorVisible = false;
                Console.Title = GameVariables.ProgramName;
                GameVariables.IncrementCurrentChapter();
                GlobalMethods.StopMusic();
                _initialDialogue = _initialDialogue.WordWrap(GameVariables.DialogueWindowWidth);
                GlobalMethods.DrawDialogueBox(_initialDialogue.Split('\n'), "Tutorial");
            } catch (Exception error) {
                string.Format("Error occured! Note levels should be all placed in: {0}", GameVariables.LevelDirectory).WriteToConsole(ItemColors.BadMessage);
                "Also Note that \"Tutorial 1\" *IS* a required level!".WriteToConsole(ItemColors.BadMessage);
                string.Format("Error: {0}", error).WriteToConsole(ItemColors.BadMessage);
            }
        }

        /// <summary>
        /// Update any game objects that need constant updates through the game
        /// </summary>
        private static void RunUpdatesAndDraws() {
            for (int i = GameVariables.GameObjectsList.Count - 1; i >= 0; i--) {
                var item = GameVariables.GameObjectsList[i];

                if (item.Enabled) {
                    item.Update();
                    //making sure the player didn't pause
                    if (!GameVariables.GamePaused) {
                        item.Draw();
                    }
                }
            }
        }

        /// <summary>
        /// draw all objects that have a draw method attached to them
        /// </summary>
        private static void RunInitialDraws() {
            GameVariables.GameObjectsList.ForEach(obj => obj.InitialDraw = true);

            for (int i = GameVariables.GameObjectsList.Count - 1; i >= 0; i--) {
                if (GameVariables.GameObjectsList[i].Enabled) {
                    GameVariables.GameObjectsList[i].Draw();
                }
            }

            GameVariables.GameObjectsList.ForEach(obj => obj.InitialDraw = false);
        }

        /// <summary>
        /// Deletes all the current level data and goes to the next one on the stack.
        /// </summary>
        private static void GoToNextLevel() {
            var test = _levelCache.FirstOrDefault(item => item.Key.LevelName.Equals(GameVariables.CurrentLevel.LevelName)).Key;

            #region cache level
            if (GameVariables.CurrentLevel != null) {
                //Need to position things back to normal before caching it
                GameVariables.GameObjectsList.ForEach(obj => obj.Position = new Point(obj.Position.X - GameVariables.LevelBorder.Left, obj.Position.Y));

                if (test == null) {
                    _levelCache.Add(GameVariables.CurrentLevel, new List<GameObject>(GameVariables.GameObjectsList));
                } else {
                    _levelCache[test] = new List<GameObject>(GameVariables.GameObjectsList);
                }
            }
            #endregion

            Level previousLevel = GameVariables.CurrentLevel;

            //Check dungeons
            if (GameVariables.NextLevel.Equals("EXIT") &&
                GameVariables.DungeonStack.Count > 0) {
                GameVariables.NextLevel = GameVariables.DungeonStack.Pop();
            }

            var cachedLevel = _levelCache.FirstOrDefault(item => item.Key.LevelName.Equals(GameVariables.NextLevel));

            //Generate map on screen according to what level should be drawn
            if (cachedLevel.Key != null) {
                GameVariables.CurrentLevel = cachedLevel.Key;
                GameVariables.CurrentLevel.CreateLevel(previousLevel, new List<GameObject>(cachedLevel.Value));
            } else if (ReadLevel()) {
                //Or create it since it's never been visited yet
                GameVariables.CurrentLevel.CreateLevel(previousLevel);
            }

            //Get the space needed for centering the map
            int halfGapSpace = 0;
            if (GameVariables.CurrentLevel.LevelWidth < GameVariables.MinimumHudWidth) {
                halfGapSpace = (GameVariables.MinimumHudWidth - GameVariables.CurrentLevel.LevelWidth) / 2;
            }

            //Set the new level border
            GameVariables.LevelBorder = new Box(halfGapSpace, GameVariables.CurrentLevel.LevelWidth + halfGapSpace,
                                                0, GameVariables.CurrentLevel.LevelHeight);

            //Set the window, position things to the center of the screen, and then draw it all
            SetWindowSize();
            GameVariables.GameObjectsList.ForEach(obj => obj.Position = new Point(obj.Position.X + GameVariables.LevelBorder.Left, obj.Position.Y));
            RunInitialDraws();
        }

        /// <summary>
        /// Read the level data and create the level
        /// </summary>
        /// <returns>Whether the file exists and was able to be read successfully</returns>
        private static bool ReadLevel() {
            string currLevelFileLocation;

            if (File.Exists(currLevelFileLocation = String.Format("{0}\\{1}.txt", GameVariables.LevelDirectory, GameVariables.NextLevel))) {
                var levelData = new List<string>();

                foreach (var item in File.ReadLines(currLevelFileLocation)) {
                    levelData.Add(item);
                }

                GameVariables.CurrentLevel = new Level(GameVariables.NextLevel, levelData.ToArray());
                return true;
            } else {
                return false;
            }
        }

        /// <summary>
        /// Change the window size and buffer. Buffer always needs +1 height due to very bottom-right corner not rendering otherwise.
        /// </summary>
        private static void SetWindowSize() {
            int width = (GameVariables.CurrentLevel.LevelWidth < GameVariables.MinimumHudWidth ? GameVariables.MinimumHudWidth : GameVariables.CurrentLevel.LevelWidth);
            int height = GameVariables.CurrentLevel.LevelHeight + GameVariables.PlayerAvatarHeightRequirement + GameVariables.HudBufferSpace;

            // Just in case the HUD goes <30 or <20 for some reason or something, make a minimum
            if (width < 30) {
                width = 30;
            }
            if (height < 20) {
                height = 20;
            }

            GameVariables.HudPosition = new Point(0, height - GameVariables.PlayerAvatarHeightRequirement - GameVariables.HudBufferSpace);
            Console.SetWindowSize(width, height);
            Console.SetBufferSize(width, height);
        }

        /// <summary>
        /// When there are no more players on the screen, it's game over!
        /// </summary>
        private static void GameOver() {
            string[] texts = new string[] {"Game Over!!!",
                                           "Press Enter to exit."};

            DrawGameResultStrings(texts, ItemColors.BadMessage);
        }

        /// <summary>
        /// Player has beaten all the levels!
        /// </summary>
        private static void GameWin() {
            string[] texts = new string[] {"You won!!!",
                                           "Press Enter to exit."};

            DrawGameResultStrings(texts, ItemColors.GoodMessage);
        }

        /// <summary>
        /// Draw all the strings down the center of the screen
        /// </summary>
        /// <param name="texts">An array of strings to draw down the center of the screen</param>
        /// <param name="textColor">The color to draw the text</param>
        private static void DrawGameResultStrings(string[] texts, Color textColor) {
            Console.Clear();
            Console.Title += String.Format(" {0}", texts[0]);

            //Set cursor to center of screen height
            Console.SetCursorPosition(0, Console.WindowHeight / 2 - texts.Length / 2);

            foreach (string text in texts) {
                Console.SetCursorPosition(Console.WindowWidth / 2 - text.Length / 2,
                                          Console.CursorTop);
                text.WriteToConsole(textColor);
            }

            while (Console.ReadKey().Key != ConsoleKey.Enter) ;
        }
    }
}
