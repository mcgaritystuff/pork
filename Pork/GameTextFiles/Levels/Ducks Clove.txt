﻿TTTWWWWWWWWWWWWWWWWWWWWWWTWWWWWWWWWWWWWWWTTTWWWWWWW
TTTT                    TTT             TTTTT     X
WB          T          TTTTT              B       T
W          TTT           B                       TT
W         TTTTT                  T              TTT
T           B           T       TTT               B
TT                     TTT     TTTTT              X
W                     TTTTT      B            ~~~~~
~~~     ~~~~            B             ~~~~~~~~~~~~~
~~~~~~~~~~~~~~~                ~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~B    B~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~B    B~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~B    B~~~~~~~~~~~~~~~~~~~~~   X
W ~~~~~~~  ~~~~~~~~~B    B~~~~~~~~~~~~~           X
W     T       ~~~~~~B    B~~~~~~                  X
W    TTT        S                       T         X
W   TTTTT                  1           TTT        X
W     B                               TTTTT       X
W                                       B         X
W                                                 X
WXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXW
ENDLEVEL

WEST=NONE
EAST=River Side
NORTH=NONE
SOUTH=The Cave
DUNGEON=NONE

#NPC number = NPC Type = NPC Name
1=RegularNpcCharacter=LuckTheDuck

SIGN=Bridge to nowhere