﻿WXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXWXXXXW
X                                            W    X
X                                            W    X
X                                            W    X
X           1                                W    X
X                                       HHH  W    X
X                                       HHH  W    X
X                                       HHH  W    X
X                                            W    X
X           HHH                              W    X
X           HHH                  2           W    X
X           HHH                              W    X
X                                            W    X
X                                      HH    W    X
X                ~~~~~~                HH    W    X
X                ~~~~~~                      W    X
WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW    X
X                                                 X
X                                                 X
X                                                 X
WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW
ENDLEVEL

WEST=SW Horse Pen
EAST=Road South
NORTH=NE Horse Pen
SOUTH=NONE
DUNGEON=NONE

#NPC number = NPC Type = NPC Name
1=RegularNpcCharacter=StalinStallion
2=RegularNpcCharacter=RockinBronco