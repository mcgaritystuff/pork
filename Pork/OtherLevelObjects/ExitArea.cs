﻿using System.Linq;

namespace Pork {
    public class ExitArea : GameObject {
        public bool Dungeon { private set; get; }
        public IPlayer CollidingPlayer { private set; get; }
        public Point ExitMapSide { private set; get; }

        public string NameOfExitLevel { private set; get; }

        /// <summary>
        /// Draw an exit object in the given directional point
        /// </summary>
        /// <param name="position">Placement of the exit</param>
        /// <param name="nameOfExitLevel">Level that it should exit to</param>
        /// <param name="exitMapSide">Which side of the map it's located (North, east, south west)</param>
        /// <param name="dungeon">If it's a dungeon, direction don't really matter</param>
        public ExitArea(Point position, string nameOfExitLevel, Point exitMapSide, bool dungeon) {
            Position = position;
            NameOfExitLevel = nameOfExitLevel;
            Dungeon = dungeon;
            ExitMapSide = exitMapSide;

            if (ExitMapSide.Equals(Direction.West)) {
                this.Icon = '←';
            } else if (ExitMapSide.Equals(Direction.East)) {
                this.Icon = '→';
            } else if (ExitMapSide.Equals(Direction.South)) {
                this.Icon = '↓';
            } else if (ExitMapSide.Equals(Direction.North)) {
                this.Icon = '↑';
            } else {
                this.Icon = '■';
            }

            this.ObjectColor = ItemColors.ExitArea;
        }

        /// <summary>
        /// Update the exit object for if it collides w/ the player
        /// </summary>
        public override void Update() {
            base.Update();

            if (CollidingPlayer != null) {
                if (!(CollidingPlayer as GameObject).Position.Equals(this.Position)) {
                    CollidingPlayer = null;
                    _alwaysDrawObject = true;
                    Draw();
                    _alwaysDrawObject = false;
                } else if (!CollidingPlayer.JustSpawned) {
                    GameVariables.NextLevel = NameOfExitLevel;
                }
            }

            CollidingPlayer = (IPlayer)GameVariables.GameObjectsList.FirstOrDefault(obj => obj is IPlayer && obj.Position.Equals(this.Position));
        }

        /// <summary>
        /// Draw the exit object (ignores player on initial draw)
        /// </summary>
        public override void Draw() {
            if (InitialDraw) {
                CollidingPlayer = null;
            }

            base.Draw();
        }
    }
}
