﻿using System.Drawing;

namespace Pork {
    public class QuestWall : Wall {
        int _chapter;
        bool _showAtChapter;

        /// <summary>
        /// Create a wall that can be created or removed based on a chapter change
        /// </summary>
        /// <param name="position">Position to put the wall</param>
        /// <param name="icon">Icon to display</param>
        /// <param name="color">Color of the quest wall</param>
        /// <param name="chapter">Chapter to display/hide on</param>
        /// <param name="showAtChapter">Whether to display or hide the quest wall after the chapter</param>
        public QuestWall(Point position, char icon, Color color, int chapter, bool showAtChapter) : base(position, icon, color) {
            _chapter = chapter;
            _showAtChapter = showAtChapter;

            GameVariables.IncrementCurrentChapter += (() => Enabled = GlobalMethods.ContinueUpdateOrDraw(this, _showAtChapter, _chapter));
            Enabled = GlobalMethods.ContinueUpdateOrDraw(this, _showAtChapter, _chapter);
        }
    }
}
