﻿using System.Drawing;

namespace Pork {
    public class Fire : GameObject, ISolid {
        public bool SolidActivated { private set; get; }

        private Color _color1, _color2;
        private readonly int _colorChangeTimer = 60;
        private int _currentColorTime = 0;
        private static bool swap = false;

        /// <summary>
        /// Create a blinking fire object
        /// </summary>
        /// <param name="position">Where to place the fire</param>
        /// <param name="icon">Icon for the object</param>
        /// <param name="color1">blinking color #1</param>
        /// <param name="color2">blinking color #2</param>
        public Fire(Point position, char icon, Color color1, Color color2) {
            Position = position;
            Icon = icon;
            _color1 = (swap ? color2 : color1);
            _color2 = (swap ? color1 : color2);
            swap = !swap;
            ObjectColor = _color1;
            _alwaysDrawObject = true;
            SolidActivated = true;
        }

        /// <summary>
        /// Draw the blinking fire object
        /// </summary>
        public override void Draw() {
            base.Draw();
            if (++_currentColorTime >= _colorChangeTimer) {
                if (ObjectColor == _color1) {
                    ObjectColor = _color2;
                } else {
                    ObjectColor = _color1;
                }
                _currentColorTime = 0;
            }
        }
    }
}
