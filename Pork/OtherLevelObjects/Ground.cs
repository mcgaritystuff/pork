﻿using System.Drawing;
using System.Linq;

namespace Pork {
    public class Ground : GameObject, ISolid {
        public bool SolidActivated { private set; get; }

        private GameObject _touchingObj;

        /// <summary>
        /// Create a plain ground object
        /// </summary>
        /// <param name="position">Place of the object</param>
        /// <param name="icon">Icon for the object</param>
        /// <param name="color">Color of the object</param>
        public Ground(Point position, char icon, Color color) {
            Position = position;
            Icon = icon;
            ObjectColor = color;
            SolidActivated = false;
        }

        /// <summary>
        /// Update accordingly for if something is touching it or if it needs to be redrawn (i.e. something's on top of it)
        /// </summary>
        public override void Update() {
            if (_touchingObj != null && 
                (!_touchingObj.Position.Equals(this.Position) || _touchingObj.WaitingRemoval)) {

                _touchingObj = null;
                _alwaysDrawObject = true;
                Draw();
                _alwaysDrawObject = false;
            }

            _touchingObj = GameVariables.GameObjectsList.FirstOrDefault(obj => obj != this && obj.Position.Equals(this.Position));
        }
    }
}
