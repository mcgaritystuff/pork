﻿using System.Drawing;

namespace Pork {
    public class Wall : GameObject, ISolid {
        public bool SolidActivated { private set; get; }

        /// <summary>
        /// Create a solid wall
        /// </summary>
        /// <param name="position">Where the wall is</param>
        /// <param name="icon">Icon to use</param>
        /// <param name="color">Color of the wall</param>
        public Wall(Point position, char icon, Color color) {
            Position = position;
            Icon = icon;
            ObjectColor = color;
            SolidActivated = true;
        }
    }
}
