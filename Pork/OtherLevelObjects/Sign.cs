﻿using System;
using System.Drawing;

namespace Pork {
    public class Sign : GameObject, ISolid, IInteractiveObject {
        public bool SolidActivated { private set; get; }

        private string _message;

        /// <summary>
        /// Create a sign post
        /// </summary>
        /// <param name="position">Location of the sign</param>
        /// <param name="icon">Icon to display</param>
        /// <param name="color">Color of the sign</param>
        /// <param name="message">Message the sign contains</param>
        public Sign(Point position, char icon, Color color, string message) {
            Position = position;
            Icon = icon;
            ObjectColor = color;
            SolidActivated = true;
            _message = message;
        }

        /// <summary>
        /// Display the sign message when the player interacts w/ it
        /// </summary>
        /// <param name="player">The player interacting with the sign</param>
        public void Interact(IPlayer player) {
            if (Enabled) {
                //Reset so that the dialogue text position is correctly written when wordwrapped
                Console.SetCursorPosition(0, 0); 

                if (!String.IsNullOrEmpty(_message)) {
                    _message = _message.WordWrap(GameVariables.DialogueWindowWidth);
                    string[] dialogueLines = _message.Split('\n');
                    GlobalMethods.DrawDialogueBox(dialogueLines, "Posted Sign");
                }
            }
        }
    }
}
