﻿using System;
using System.Drawing;

namespace Pork {
    public class Water : GameObject, ISolid, IInteractiveObject {
        public bool SolidActivated { private set; get; }

        /// <summary>
        /// A water object the player can interact with
        /// </summary>
        /// <param name="position">The position of the water</param>
        /// <param name="icon">The icon to use</param>
        /// <param name="color">Color of the water</param>
        public Water(Point position, char icon, Color color) {
            Position = position;
            Icon = icon;
            ObjectColor = color;
            SolidActivated = true;
        }

        /// <summary>
        /// Player interaction with the water
        /// </summary>
        /// <param name="player">The player interacting with it</param>
        public void Interact(IPlayer player) {
            if (Enabled) {
                //Reset so that the dialogue text position is correctly written when wordwrapped
                Console.SetCursorPosition(0, 0); 

                GlobalMethods.DrawDialogueBox(new string[] { "Slurp Slurp!!" }, player.PlayerName);
            }
        }
    }
}
