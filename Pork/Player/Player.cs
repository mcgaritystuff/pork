﻿using System;
using System.Drawing;
using System.Linq;

namespace Pork {
    public class Player : GameObject, IPlayer {
        public bool JustSpawned { private set; get; }
        public uint Currency { private set; get; }
        public string PlayerName { set; get; }
        public string[] AvatarImage { set; get; }
        public int Health { private set; get; }
        public int MaxHealth { set; get; }
        public IWeapon CurrentWeapon { private set; get; }
        public Point CreatureDirection { private set; get; }
        public Inventory PlayerInventory { private set; get; }

        private event Action<IPlayer> _updateHud;
        private string _dyingSound = "PlayerDeath.wav";

        /// <summary>
        /// Create a new player
        /// </summary>
        /// <param name="startPosition">The position the player starts in</param>
        /// <param name="icon">The icon for the player</param>
        /// <param name="color">The color for the player</param>
        public Player(Point startPosition, char icon, Color color) {
            Position = startPosition;
            Icon = icon;
            CreatureDirection = Direction.East;
            ObjectColor = color;
            JustSpawned = true;
            Health = MaxHealth = 100;

            _alwaysDrawObject = true;
            _updateHud += GlobalMethods.DrawPlayerHud;
            PlayerInventory = new Inventory(this);

            // Always give the player a pistol to start with
            var pistol = GameWeapons.PistolNormal(Point.Zero);
            pistol.Hide();
            PlayerInventory.AddWeapon(pistol);
            SwapWeapon(pistol);
        }

        /// <summary>
        /// Force the player into a position on the map.
        /// </summary>
        /// <param name="newPosition">The position on the map to put the player</param>
        public void ForceNewPosition(Point newPosition) {
            JustSpawned = true;
            Position = newPosition;
        }

        /// <summary>
        /// Increment the player's funds
        /// </summary>
        /// <param name="value">How much currency to add</param>
        public void GainCurrency(uint value) {
            try {
                Currency = checked(value + Currency);
                _updateHud?.Invoke(this);
            } catch {
                return;
            }
        }

        /// <summary>
        /// Spend some money on sumfin!
        /// </summary>
        /// <param name="value">The amount the player will decrement</param>
        /// <returns></returns>
        public bool SpendCurrency(uint value) {
            try {
                Currency = checked(Currency - value);
                _updateHud?.Invoke(this);
                return true;
            } catch {
                return false;
            }
        }

        /// <summary>
        /// Change the player's weapon
        /// </summary>
        /// <param name="newWeapon">The new weapon for the player to use</param>
        public void SwapWeapon(IWeapon newWeapon) {
            CurrentWeapon = newWeapon;

            if (_updateHud != null && AvatarImage != null && !PlayerInventory.Opened) {
                _updateHud(this);
            }
        }

        /// <summary>
        /// Update the player
        /// </summary>
        public override void Update() {
            base.Update();

            CheckPlayerInput();
            CurrentWeapon.Update();
        }

        /// <summary>
        /// Draw the palyer on the map
        /// </summary>
        public override void Draw() {
            base.Draw();
            if (InitialDraw) {
                _updateHud(this);
            }
        }

        /// <summary>
        /// Heal the player some health
        /// </summary>
        /// <param name="health">How much to heal</param>
        public void Heal(int health) {
            // Heal up to the max, not past it
            if ((Health += health) > MaxHealth) {
                Health = MaxHealth;
            }
        }

        /// <summary>
        /// Take some damage, ouch!
        /// </summary>
        /// <param name="damage">The amount of damage to take</param>
        public void TakeDamage(int damage) {
            Health -= damage;

            if (Health <= 0) {
                WaitingRemoval = true;
            } else {
                GlobalMethods.PlaySound("PlayerOuch.wav");
            }

            _updateHud?.Invoke(this);
        }

        /// <summary>
        /// Destroy the player object :(
        /// </summary>
        public override void Destroy() {
            base.Destroy();

            if (GameVariables.GameObjectsList.Where(item => item is IPlayer).Count() == 0) {
                GameVariables.GameOver = true;
            }

            GlobalMethods.PlaySound(_dyingSound);
        }

        /// <summary>
        /// See what the player's trying to do
        /// </summary>
        private void CheckPlayerInput() {
            if (GameVariables.CurrentKeyPressed != ConsoleKey.NoName) {
                Point newPosition = Position;
                Point newDirection = Point.Zero;

                switch (GameVariables.CurrentKeyPressed) {
                    case Controls.MoveLeft:
                    case Controls.MoveLeftAlternative:
                        if (Position.X > GameVariables.LevelBorder.Left) {
                            newPosition = new Point(Position.X - 1, Position.Y);
                        }
                        newDirection = Direction.West;
                        break;
                    case Controls.MoveRight:
                    case Controls.MoveRightAlternative:
                        if (Position.X < GameVariables.LevelBorder.Right - 1) {
                            newPosition = new Point(Position.X + 1, Position.Y);
                        }
                        newDirection = Direction.East;
                        break;
                    case Controls.MoveDown:
                    case Controls.MoveDownAlternative:
                        if (Position.Y < GameVariables.LevelBorder.Bottom - 1) {
                            newPosition = new Point(Position.X, Position.Y + 1);
                        }
                        newDirection = Direction.South;
                        break;
                    case Controls.MoveUp:
                    case Controls.MoveUpAlternative:
                        if (Position.Y > GameVariables.LevelBorder.Top) {
                            newPosition = new Point(Position.X, Position.Y - 1);
                        }
                        newDirection = Direction.North;
                        break;
                    case Controls.ShootKey:
                        CurrentWeapon.Shoot(CreatureDirection,
                                            Position + CreatureDirection,
                                            false);
                        return;
                    case Controls.ActionKey:
                        IInteractiveObject item = GameVariables.GameObjectsList
                                                               .FirstOrDefault(obj => obj is IInteractiveObject && 
                                                                                      obj.Position.Equals(Position + CreatureDirection)) as IInteractiveObject;
                        if (item != null) {
                            item.Interact(this);
                        }
                        break;
                    case Controls.InventoryKey:
                        PlayerInventory.ViewInventory();
                        break;
                    case Controls.JournalKey:
                        Console.Clear();
                        GameVariables.GamePaused = true;
                        QuestLog.PrintAllQuests();
                        break;
                }

                // If the player's direction is updated from movement, make sure they aren't colliding with a wall
                if (!newDirection.Equals(Point.Zero) &&
                    !GlobalMethods.CheckForSolidCollisions(newPosition)) {
                    Position = newPosition;
                    JustSpawned = false;
                }

                // Setup the player's direction if it's still updated
                if (!newDirection.Equals(Point.Zero)) {
                    CreatureDirection = newDirection;
                }
            }
        }
    }
}
