﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Pork {
    public static class QuestLog {
        private struct Quest {
            public int ChapterNumber;
            public string Text;
            public string CreatorName;
        }

        private static string _questFormatter = " * From {1}: {2}";
        private static List<Quest> _quests = new List<Quest>();

        /// <summary>
        /// Add a new quest!
        /// </summary>
        /// <param name="chapter">The chapter number of the quest</param>
        /// <param name="text">The text for the journal</param>
        /// <param name="creatorName">Who created the quest</param>
        public static void AddQuest(int chapter, string text, string creatorName) {
            _quests.Add(new Quest() { ChapterNumber = chapter, Text = text, CreatorName = creatorName });
        }

        /// <summary>
        /// Get all quests the player has
        /// </summary>
        /// <returns></returns>
        public static List<string> GetAllQuests() {
            List<string> questList = new List<string>();
            _quests.Where(q => q.ChapterNumber <= GameVariables.CurrentChapter)
                   .ToList()
                   .ForEach(q => questList.Add(string.Format(_questFormatter, q.ChapterNumber, q.CreatorName, q.Text)));

            return questList;
        }

        /// <summary>
        /// Get a list of specific quests based on the chapter number
        /// </summary>
        /// <param name="chapter">The chapter number</param>
        /// <returns>The quest(s) information</returns>
        public static List<string> GetSpecificQuests(int chapter) {
            var quests = _quests.Where(q => q.ChapterNumber == chapter).ToList();
            List<string> questList = new List<string>();
            quests.ForEach(q => questList.Add(string.Format(_questFormatter, q.ChapterNumber, q.CreatorName, q.Text)));

            return questList;
        }

        /// <summary>
        /// Get a list of specific quests based on the creator name
        /// </summary>
        /// <param name="chapter">The chapter number</param>
        /// <returns>The quest(s) information</returns>
        public static List<string> GetSpecificQuests(string creatorName) {
            var quests = _quests.Where(q => q.CreatorName == creatorName).ToList();
            List<string> questList = new List<string>();
            quests.ForEach(q => questList.Add(string.Format(_questFormatter, q.ChapterNumber, q.CreatorName, q.Text)));

            return questList;
        }

        /// <summary>
        /// Print all quests available
        /// </summary>
        public static void PrintAllQuests() {
            "Quests In Progress".WriteToConsole(ItemColors.BadMessage);
            " | ".WriteToConsole();
            "Quests Completed".WriteToConsole(ItemColors.GoodMessage);

            Console.WriteLine();
            Console.WriteLine();

            foreach (var quest in _quests.Where(q => q.ChapterNumber < GameVariables.CurrentChapter)) {
                string.Format(_questFormatter, quest.ChapterNumber, quest.CreatorName, quest.Text)
                      .WordWrap(false)
                      .WriteToConsole(quest.ChapterNumber >= GameVariables.CurrentChapter - 1 ?
                                                                   ItemColors.BadMessage : ItemColors.GoodMessage);
                Console.WriteLine();
            }
        }
    }
}
