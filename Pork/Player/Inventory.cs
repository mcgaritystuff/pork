﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Pork {
    public class Inventory {
        private class InventoryItem<T> {
            public Point InventoryPosition { set; get; }
            public T Item { private set; get; }
            public InventoryItem(T item) {
                Item = item;
            }
        }

        public bool Opened { private set; get; }
        private List<InventoryItem<IWeapon>> _weapons = new List<InventoryItem<IWeapon>>();
        private List<InventoryItem<IItem>> _items = new List<InventoryItem<IItem>>();
        private List<InventoryItem<IEquippable>> _equipment = new List<InventoryItem<IEquippable>>();

        private IPlayer _player;
        private Point _selectorPosition = Point.Zero;
        private Box _inventoryBox; //The box representing where items are in the inventory
        private int _inventoryBottom; //Farthest the inventory can stretch to avoid conflicting with the HUD and stuff
        private string _title = "Inventory";

        private readonly Color _unselectedColor = ItemColors.DefaultConsoleColor;
        private readonly Color _selectedColor = ItemColors.SelectedItem;

        /// <summary>
        /// Create a new inventory for a player
        /// </summary>
        /// <param name="respectivePlayer">The player who deserves an inventory</param>
        public Inventory(IPlayer respectivePlayer) {
            _player = respectivePlayer;
            if (!string.IsNullOrEmpty(_player.PlayerName)) {
                _title = string.Format("{0}'s Inventory", _player.PlayerName);
            }
        }

        /// <summary>
        /// View the player's inventory
        /// </summary>
        public void ViewInventory() {
            _inventoryBottom = Console.WindowHeight - GameVariables.PlayerAvatarHeightRequirement - GameVariables.HudBufferSpace;
            Opened = true;
            DrawInventoryOutline();

            int headerTop = Console.CursorTop;
            DrawItemLists(headerTop);

            ConsoleKey currentKeyPress = ConsoleKey.NoName;

            #region Handle player controls while viewing it
            while (currentKeyPress != Controls.InventoryKey && currentKeyPress != Controls.GameplayMenuKey) {
                currentKeyPress = Console.ReadKey(true).Key;
                switch (currentKeyPress) {
                    case Controls.MoveUp:
                    case Controls.MoveUpAlternative:
                        if (UpdateSelectorPosition(new Point(0, -1)))
                            DrawItemLists(headerTop);
                        break;
                    case Controls.MoveDown:
                    case Controls.MoveDownAlternative:
                        if (UpdateSelectorPosition(new Point(0, 1)))
                            DrawItemLists(headerTop);
                        break;
                    case Controls.MoveLeft:
                    case Controls.MoveLeftAlternative:
                        if (UpdateSelectorPosition(new Point(-1, 0)))
                            DrawItemLists(headerTop);
                        break;
                    case Controls.MoveRight:
                    case Controls.MoveRightAlternative:
                        if (UpdateSelectorPosition(new Point(1, 0)))
                            DrawItemLists(headerTop);
                        break;
                    case Controls.InventoryEquipUseKey:
                        SelectItem();
                        DrawInventoryOutline();
                        DrawItemLists(headerTop);
                        break;
                    case Controls.InventorySpecificationsKey:
                        GetItemInformation();
                        break;
                    default:
                        continue;
                }

                DrawItemLists(headerTop);
            }
            #endregion
            
            GameVariables.RefreshScreen = true;
            Opened = false;
        }

        /// <summary>
        /// Add a weapon to the inventory
        /// </summary>
        /// <param name="weapon">The weapon to add</param>
        public void AddWeapon(IWeapon weapon) {
            weapon.Hide();
            _weapons.Add(new InventoryItem<IWeapon>(weapon));
        }

        /// <summary>
        /// Add something like armor or whatnot to the inventory
        /// </summary>
        /// <param name="item">The item to add</param>
        public void AddEquippableItem(IEquippable item) {
            item.Hide();
            _equipment.Add(new InventoryItem<IEquippable>(item));
        }

        /// <summary>
        /// Add an item like a potion to the inventory
        /// </summary>
        /// <param name="item">The item to add</param>
        public void AddItem(IItem item) {
            item.Hide();
            _items.Add(new InventoryItem<IItem>(item));
        }

        /// <summary>
        /// Draw the inventory outline/border and stuff
        /// </summary>
        private void DrawInventoryOutline() {
            Console.Clear();
            GlobalMethods.DrawPlayerHud(_player);

            #region drawing headers
            //Draw title
            Console.SetCursorPosition(Console.WindowWidth / 2 - _title.Length / 2, 0);
            _title.WriteToConsole(ItemColors.GoodMessage);

            //Draw equipment/items headers
            Console.SetCursorPosition(0, Console.CursorTop + 1);
            (new string('-', Console.WindowWidth)).WriteToConsole(ItemColors.PlainUI);
            string equipmentText = "Equipment";
            string itemText = "Items";

            #endregion

            //equipment
            Console.SetCursorPosition(Console.WindowWidth / 4 - equipmentText.Length / 2, Console.CursorTop);
            equipmentText.WriteToConsole(ItemColors.PlainUI);

            int positionBeforeSeparators = Console.CursorTop;

            for (int i = Console.CursorTop; i < _inventoryBottom; i++) {
                Console.SetCursorPosition(Console.WindowWidth / 2, i);
                '|'.WriteToConsole(ItemColors.PlainUI);
            }

            //items
            Console.SetCursorPosition(Console.WindowWidth / 4 * 3 - itemText.Length / 2, positionBeforeSeparators);
            itemText.WriteToConsole(ItemColors.PlainUI);

            Console.SetCursorPosition(0, Console.CursorTop + 1);
            (new string('-', Console.WindowWidth)).WriteToConsole(ItemColors.PlainUI);
        }

        /// <summary>
        /// Draw all the item list(s)
        /// </summary>
        /// <param name="headerTop">The top of the header for the item lists</param>
        private void DrawItemLists(int headerTop) {
            int startX = 0;
            Point _drawPosition = Point.Zero;
            Console.SetCursorPosition(startX, headerTop);
            int _maxHeight = 0;
            int _width = 1;

            //TODO: SCROLLING FEATURE. IF WE OVERLOAD WEAPONS, THIS WILL CRASH THE CONSOLE
            // Print all weapons
            for (int i = 0; i < _weapons.Count; i++, _drawPosition += new Point(0, 1)) {
                var currentWeapon = _weapons[i];
                currentWeapon.InventoryPosition = _drawPosition;

                Console.SetCursorPosition(startX, Console.CursorTop + 1);
                string.Format(" [{0}] {1}", _player.CurrentWeapon == currentWeapon.Item ? '*' : ' ',
                    currentWeapon.Item.EquipmentName).WriteToConsole((_selectorPosition.Equals(_drawPosition) ? _selectedColor : _unselectedColor));
            }

            // Space between weapons/equipment
            Console.SetCursorPosition(startX, Console.CursorTop + 1);

            //TODO: SCROLLING FEATURE. IF WE OVERLOAD EQUIPMENT, THIS WILL CRASH THE CONSOLE
            // Print all the equipment
            for (int i = 0; i < _equipment.Count; i++, _drawPosition += new Point(0, 1)) {
                var currentItem = _equipment[i];
                currentItem.InventoryPosition = _drawPosition;

                Console.SetCursorPosition(startX, Console.CursorTop + 1);
                string.Format(" [{0}] {1}", currentItem.Item.IsEquipped ? '*' : ' ',
                    currentItem.Item.EquipmentName).WriteToConsole((_selectorPosition.Equals(_drawPosition) ? _selectedColor : _unselectedColor));
            }
                        
            // Get ready to draw all the items
            _maxHeight = _drawPosition.Y;
            _drawPosition = new Point(1, 0);
            if (_items.Count > 0) {
                _width++;
            }

            // Draw all items found
            startX = Console.WindowWidth / 2 + 1;
            Console.SetCursorPosition(startX, headerTop);
            for (int i = 0; i < _items.Count; i++, _drawPosition += new Point(0, 1)) {
                var currentItem = _items[i];
                currentItem.InventoryPosition = _drawPosition;

                if (Console.CursorTop + 1 >= _inventoryBottom - 1) {
                    _width++;
                    _drawPosition = new Point(_drawPosition.X + 1, 0);
                    startX += Console.WindowWidth / 4;
                    Console.SetCursorPosition(startX, headerTop + 1);
                } else {
                    Console.SetCursorPosition(startX, Console.CursorTop + 1);
                }
                string.Format(" [{0}] {1}", false /*Selector position*/ ? '*' : ' ',
                    currentItem.Item.ItemName).WriteToConsole((_selectorPosition.Equals(_drawPosition) ? _selectedColor : _unselectedColor));
            }

            if (_drawPosition.Y > _maxHeight) {
                _maxHeight = _drawPosition.Y;
            }

            _inventoryBox = new Box(0, _width, 0, _maxHeight);
        }

        /// <summary>
        /// Get some information (UI) about a particular selected item
        /// </summary>
        private void GetItemInformation() {
            List<string> lines = new List<string>();
            Box infoBox = new Box(Console.WindowWidth / 2 - GameVariables.MinimumHudWidth / 3, Console.WindowWidth / 2 + GameVariables.MinimumHudWidth / 3, 0, 0);
            string title = "";

            var selectedWep = _weapons.FirstOrDefault(wep => wep.InventoryPosition.Equals(_selectorPosition));
            var selectedEquipment = _equipment.FirstOrDefault(item => item.InventoryPosition.Equals(_selectorPosition));
            var selectedItem = _items.FirstOrDefault(item => item.InventoryPosition.Equals(_selectorPosition));

            Console.SetCursorPosition(infoBox.Left + 2, infoBox.Top + 1);

            // if/else statements to determine what's printed out
            if (selectedWep != null) {
                lines.Add("Info: " + selectedWep.Item.EquipmentDescription);
                lines.Add("Damage: " + selectedWep.Item.WeaponDamage);
                lines.Add("Bullet Speed: " + selectedWep.Item.BulletSpeed);
                if (selectedWep.Item.MagicAttributes != null) {
                    selectedWep.Item.MagicAttributes.ForEach(attr => lines.Add(attr.Description));
                }

                title = selectedWep.Item.EquipmentName;
            } else if (selectedEquipment != null) {
                lines.Add("Info: " + selectedEquipment.Item.EquipmentDescription);
                if (selectedEquipment.Item is Armor) {
                    lines.Add("Armor: " + (selectedEquipment.Item as Armor).ArmorPoints);
                }
                if (selectedEquipment.Item.MagicAttributes != null) {
                    selectedEquipment.Item.MagicAttributes.ForEach(attr => lines.Add(attr.Description));
                }

                title = selectedEquipment.Item.EquipmentName;

            } else if (selectedItem != null) {
                var wrappedDescription = selectedItem.Item.ItemDescription.WordWrap(infoBox.Left + infoBox.Right - 2).Split('\n');
                lines = wrappedDescription.ToList();

                title = selectedItem.Item.ItemName;
            }

            int height = lines.Count + 2;
            if (height % 2 != 0) {
                height++;
            }
            infoBox = new Box(Console.WindowWidth / 2 - GameVariables.MinimumHudWidth / 3,
                              Console.WindowWidth / 2 + GameVariables.MinimumHudWidth / 3,
                              Console.WindowHeight / 2 - height / 2,
                              Console.WindowHeight / 2 + height / 2);
            // The info box is a dialogue box (has the +++ pattern)
            GlobalMethods.DrawMainDialogueBox(infoBox, title);

            for (int i = 0; i < lines.Count; i++) {
                Console.SetCursorPosition(infoBox.Left + 2, infoBox.Top + i + 1);
                lines[i].WriteToConsole(ItemColors.PlainUI);
            }
            while (Console.ReadKey(true).Key != Controls.ActionKey) ;

            Console.Clear();
            DrawInventoryOutline();
            int headerTop = Console.CursorTop;
            DrawItemLists(headerTop);
        }

        /// <summary>
        /// Select the item currently highlighted
        /// </summary>
        private void SelectItem() {
            var selectedWep = _weapons.FirstOrDefault(wep => wep.InventoryPosition.Equals(_selectorPosition));
            var selectedEquipment = _equipment.FirstOrDefault(item => item.InventoryPosition.Equals(_selectorPosition));
            var selectedItem = _items.FirstOrDefault(item => item.InventoryPosition.Equals(_selectorPosition));

            // Depending on the type of item, equip it or use it
            if (selectedWep != null) {
                _player.CurrentWeapon.UnequipItem(_player);
                _player.SwapWeapon(selectedWep.Item);
                selectedWep.Item.EquipItem(_player);
            } else if (selectedEquipment != null) {
                _equipment.ForEach(item => {
                    if (item.Item.IsEquipped) {
                        item.Item.UnequipItem(_player);
                    }
                });
                selectedEquipment.Item.EquipItem(_player);
            } else if (selectedItem != null) {
                selectedItem.Item.UseItem();
                if (selectedItem.Item.WaitingRemoval) {
                    _items.Remove(selectedItem);
                    _selectorPosition = Point.Zero;
                }
            }
        }

        /// <summary>
        /// Update the item that's currently highlighted
        /// </summary>
        /// <param name="newPosition">The new item (x,y) to select</param>
        /// <returns>True if a new item is selected, false otherwise</returns>
        private bool UpdateSelectorPosition(Point newPosition) {
            Point newSelectorPosition = _selectorPosition + newPosition;
            if (newSelectorPosition.X < _inventoryBox.Left || newSelectorPosition.X > _inventoryBox.Right - 1 ||
                newSelectorPosition.Y < _inventoryBox.Top || newSelectorPosition.Y > _inventoryBox.Bottom - 1) {
                return false;
            }

            var selectedWep = _weapons.FirstOrDefault(wep => wep.InventoryPosition.Equals(newSelectorPosition));
            var selectedEquipment = _equipment.FirstOrDefault(item => item.InventoryPosition.Equals(newSelectorPosition));
            var selectedItem = _items.FirstOrDefault(item => item.InventoryPosition.Equals(newSelectorPosition));

            if (selectedWep != null || selectedEquipment != null || selectedItem != null) {
                _selectorPosition = newSelectorPosition;
                return true;
            }

            return false;
        }
    }
}
