﻿using System.Drawing;

namespace Pork {
    class RegularNpcCharacter : CoreNPCObject {

        /// <summary>
        /// Create a regular NPC character that can talk to the player
        /// </summary>
        /// <param name="icon">Icon of the NPC</param>
        /// <param name="color">Color of the NPC</param>
        /// <param name="name">The name of the NPC</param>
        /// <param name="position">Where they are on the map</param>
        public RegularNpcCharacter(char icon, Color color, string name, Point position)
            : base(icon, color, name, position) { }

        /// <summary>
        /// Get the printed dialogue of the NPC
        /// </summary>
        /// <param name="player">Player interacting w/ the NPC</param>
        /// <returns>The dialogue text provided (and wordwrapped already)</returns>
        public override string GetDialogue(IPlayer player) {
            string dialogueText = base.GetDialogue(player);

            //Increment the current quest we're on
            if (_currentDialogue.QuestDialogue) {
                GameVariables.IncrementCurrentChapter();
            }

            return dialogueText;
        }
    }
}
