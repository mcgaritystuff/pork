﻿using System.Drawing;

namespace Pork {
    class MerchantNpcCharacter : CoreNPCObject {

        /// <summary>
        /// Create a merchant NPC character that can talk and sell/buy stuff with the player
        /// </summary>
        /// <param name="icon">Icon of the NPC</param>
        /// <param name="color">Color of the NPC</param>
        /// <param name="name">The name of the NPC</param>
        /// <param name="position">Where they are on the map</param>
        public MerchantNpcCharacter(char icon, Color color, string name, Point position)
            : base(icon, color, name, position) { }

    }
}
