﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Pork {
    abstract class CoreNPCObject : GameObject, ISolid, IInteractiveObject {
        public string NpcName { protected set; get; }
        public bool SolidActivated { protected set; get; }

        protected List<GameDialogue> _gameDialogues = new List<GameDialogue>();
        protected GameDialogue _currentDialogue;

        /// <summary>
        /// An abstract and core NPC object that should define other NPCs
        /// </summary>
        /// <param name="icon">Icon of the NPC</param>
        /// <param name="color">Color of the NPC</param>
        /// <param name="name">The name of the NPC</param>
        /// <param name="position">Where they are on the map</param>
        public CoreNPCObject(char icon, Color color, string name, Point position) {
            this.Icon = icon;
            this.ObjectColor = color;
            this.NpcName = name;
            this.Position = position;
            this.SolidActivated = true;
        }

        /// <summary>
        /// Add some NPC dialogue for convos
        /// </summary>
        /// <param name="dialogue">The dialogue to add</param>
        public void AddDialogue(GameDialogue dialogue) {
            _gameDialogues.Add(dialogue);
        }

        /// <summary>
        /// Get the NPC dialogue (wordwrapped)
        /// </summary>
        /// <param name="player">The player interacted with</param>
        /// <returns>The dialogue, otherwise a generic statement</returns>
        public virtual string GetDialogue(IPlayer player) {
            if (_gameDialogues.Count > 0) {
                _currentDialogue = _gameDialogues.FirstOrDefault(dg => dg.ChapterNumber == GameVariables.CurrentChapter);

                if (!String.IsNullOrEmpty(_currentDialogue.Dialogue)) {
                    _currentDialogue.Dialogue = _currentDialogue.Dialogue.Replace("$1", player.PlayerName);
                    _currentDialogue.Dialogue = _currentDialogue.Dialogue.ReplaceKeywords();

                    return _currentDialogue.Dialogue.WordWrap(GameVariables.DialogueWindowWidth);
                }
            }

            return "I have nothing to say.";
        }

        /// <summary>
        /// Interact with the player
        /// </summary>
        /// <param name="player">The player to interact with</param>
        public virtual void Interact(IPlayer player) {
            if (Enabled) {
                GlobalMethods.PlaySound("Hey.wav");
                string wrappedDialogue = GetDialogue(player);
                string[] dialogueLines = wrappedDialogue.Split('\n');
                GlobalMethods.DrawDialogueBox(dialogueLines, this.NpcName);
            }
        }
    }
}
