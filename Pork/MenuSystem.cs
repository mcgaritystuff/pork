﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Pork {
    public sealed class MenuSystem {

        private bool _drawLogo = false;
        
        private static string Logo = File.ReadAllText(GameVariables.LogoFile);
        private Stack<IMenu> _menuStack;

        /// <summary>
        /// Create the new menu system.
        /// </summary>
        /// <param name="mainMenu">The initial main menu to push on the stack</param>
        /// <param name="drawLogo">Draw the game logo or not</param>
        public MenuSystem(IMenu mainMenu, bool drawLogo) {
            _menuStack = new Stack<IMenu>();
            _drawLogo = drawLogo;

            try {
                PushMenu(mainMenu);
            } catch (Exception error) {
                Console.WriteLine(error.Message);
                Console.ReadLine();
            }
        }

        /// <summary>
        /// Run the menu system
        /// </summary>
        public void RunMenu() {
            try {
                while (GameVariables.CurrentGameState != GameState.Gameplay) {
                    // Clear the menu screen to "refresh" it
                    Console.Clear();
                    Console.SetCursorPosition(0, 0);

                    // Draw the logo if applicable
                    if (_drawLogo) {
                        Console.WriteLine(Logo);
                    }

                    // Top border for the menu screen
                    Console.WriteLine("\n{0}\n", new string('=', 25));

                    // Display the menu item that's on the top of the stack
                    var beforeState = GameVariables.CurrentGameState;
                    PeekMenu().DisplayMenu();

                    // If the menu being displayed changes the state, we need to break from this loop.
                    if (GameVariables.CurrentGameState != beforeState) {
                        break;
                    }

                    // Bottom of the current menu item's printout
                    Console.WriteLine("\n{0}\n", new string('=', 25));

                    // If there's another menu on the stack, the player should be able to go "back"
                    if (_menuStack.Count > 1) {
                        Console.WriteLine(String.Format("{0} - Go Back", Enum.GetName(typeof(ConsoleKey), Controls.BackKey)));
                    }

                    // The player's choice to interact w/ the menu
                    Console.Write("Choice: ");
                    GetInput();
                }

                // Be sure to keep the top level of the menu (in case the player goes back to the main menu
                // or pauses/unpauses the game). Otherwise our menu system will pop the option off and the 
                // game will break
                var originalItem = _menuStack.LastOrDefault();
                _menuStack.Clear();
                _menuStack.Push(originalItem);
            } catch (Exception error) {
                Console.WriteLine(error.Message);
                Console.ReadLine();
            }
        }

        /// <summary>
        /// Get user input to know what menu choice to select or what to do on the menu.
        /// </summary>
        private void GetInput() {
            string inputStr = Console.ReadLine();
            string backKey = Enum.GetName(typeof(ConsoleKey), Controls.BackKey);

            // If the player pushes back, go back one menu. Otherwise (At this time) use the number
            // respective to the menu option
            if (inputStr.ToUpperInvariant() == Enum.GetName(typeof(ConsoleKey), Controls.BackKey)) {
                PopMenu();
            } else {
                if (_menuStack.Count > 0) {
                    IMenu newMenu;
                    int menuChoice = 0;

                    if (!Int32.TryParse(inputStr, out menuChoice)) {
                        return;
                    }

                    newMenu = PeekMenu().SelectMenu(menuChoice);

                    if (newMenu != null) {
                        PushMenu(newMenu);
                    }
                }
            }
        }

        /// <summary>
        /// View the current menu item on the stack
        /// </summary>
        /// <returns>The curent menu being viewed</returns>
        public IMenu PeekMenu() {
            return _menuStack.Peek();
        }

        /// <summary>
        /// Pop the menu that's in view right now
        /// </summary>
        private void PopMenu() {
            if (_menuStack.Count > 1) {
                _menuStack.Pop();
            }

            Console.Title = String.Format("{0} - {1}", GameVariables.ProgramName, PeekMenu().MenuTitle);
        }

        /// <summary>
        /// Push a new menu on the stack
        /// </summary>
        /// <param name="newMenu">The menu we want to now display on the screen</param>
        private void PushMenu(IMenu newMenu) {
            _menuStack.Push(newMenu);
            Console.Title = String.Format("{0} - {1}", GameVariables.ProgramName, newMenu.MenuTitle);
        }
    }
}
