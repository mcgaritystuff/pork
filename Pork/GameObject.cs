﻿using System;
using System.Drawing;

namespace Pork {

    /// <summary>
    /// This is the key object to the game. If you want something to show up on the 
    /// game map, you must inherit this for it to be drawn/updated. Otherwise the game
    /// engine won't recognize it.
    /// </summary>
    public class GameObject : IDestroyable {
        public char Icon { protected set; get; }
        public Color ObjectColor { protected set; get; }
        public Point Position { set; get; }
        public bool Enabled { set; get; }
        public bool InitialDraw { set; protected get; }
        public bool DrawOnMap { protected set; get; }
        public bool WaitingRemoval { set; get; }

        /// <summary>
        /// Helps us determine whether we should update or redraw the object to save processing power
        /// </summary>
        private Point _previousPosition = new Point(-1, -1);

        /// <summary>
        /// goes hand-in-hand with _previousPosition to determine whether this object is now to be drawn in the new spot
        /// </summary>
        private bool _updateAndDraw = false; 

        protected bool _alwaysDrawObject = false;

        /// <summary>
        /// A game object to draw and update in the game engine
        /// </summary>
        public GameObject() {
            GameVariables.GameObjectsList.Add(this);
            DrawOnMap = true;
            Enabled = true;
        }

        /// <summary>
        /// The core update function for all game objects that need to be updated on the screen (i.e. they move!)
        /// </summary>
        public virtual void Update() {
            // Skip if it's a disabled object
            if (!Enabled) {
                return;
            }

            // Move the object if its position has changed
            if (!Position.Equals(_previousPosition)) {
                
                var collidedObj = GlobalMethods.CheckForGameObjects(_previousPosition);

                // If no "true" collisions and it's not the initial placement (-1,-1), move it.
                if (_previousPosition.X != -1 && _previousPosition.Y != -1 && 
                    (collidedObj == null || collidedObj.WaitingRemoval || !collidedObj.DrawOnMap)) {

                    try {
                        Console.SetCursorPosition(_previousPosition.X, _previousPosition.Y);
                        Console.Write(' ');
                    } catch { } //Probably a level change in size, likely nothing to concern about.

                }

                _previousPosition = Position;
                _updateAndDraw = true;
            }

            if (WaitingRemoval) {
                Destroy();
            }
        }

        /// <summary>
        /// Draw the game object on the console
        /// </summary>
        public virtual void Draw() {
            // If it shouldn't be drawn or it's disabled, skip it
            if (!DrawOnMap || !Enabled) {
                return;
            }

            // Only draw it if it's the initial draw, it's "moved", or it should be *always* drawing every frame
            if ((_updateAndDraw && !WaitingRemoval) ||
                InitialDraw || 
                (_alwaysDrawObject && !WaitingRemoval)) {

                // If the position is the very bottom-right corner, just draw it one position above (it's "out of bounds")
                // otherwise, set it as normal
                if (Position.Equals(new Point(Console.WindowWidth, Console.WindowHeight))) {
                    Console.SetCursorPosition(Position.X, Position.Y - 1);
                } else {
                    Console.SetCursorPosition(Position.X, Position.Y);
                }

                Icon.WriteToConsole(ObjectColor);

                // No longer need to draw it again, set this to false to avoid future drawings until it moves
                _updateAndDraw = false;
            }
        }

        /// <summary>
        /// Destroy the object and remove it from the objects list (it no longer should exist!)
        /// </summary>
        public virtual void Destroy() {

            // Erase the object from the screen since it should be destroyed
            if (Position.X != -1 && Position.Y != -1) {
                try {
                    Console.SetCursorPosition(_previousPosition.X, _previousPosition.Y);
                    Console.Write(' ');
                } catch (Exception e) {
                    throw new Exception(String.Format("Could not update {0}. Error message: {1}", this.GetType().Name, e.Message));
                }
            }

            // Now remove the object from the list of Game Objects
            GameVariables.GameObjectsList.Remove(this);
        }
    }
}
