﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Console = Colorful.Console;

namespace Pork {
    public static class GameExtensions {

        // This is in support of the method ReplaceKeywords below. This is so if the developer changes the key
        // for an control, the printout is always correct in the game (requires no modification to npc dialogues, etc.)
        private static Dictionary<string, string> _replacementStrings = new Dictionary<string, string>() {
            {"$ACTION", Enum.GetName(typeof(ConsoleKey), Controls.ActionKey).SplitByCapitalLetters()},
            {"$GAMEPLAYMENU", Enum.GetName(typeof(ConsoleKey), Controls.GameplayMenuKey).SplitByCapitalLetters()},
            {"$JOURNAL", Enum.GetName(typeof(ConsoleKey), Controls.JournalKey).SplitByCapitalLetters()},
            {"$PAUSEGAME", Enum.GetName(typeof(ConsoleKey), Controls.PauseGameKey).SplitByCapitalLetters()},
            {"$SHOOT", Enum.GetName(typeof(ConsoleKey), Controls.ShootKey).SplitByCapitalLetters()},

            {"$LEFT1", Enum.GetName(typeof(ConsoleKey), Controls.MoveLeft).SplitByCapitalLetters()},
            {"$LEFT2", Enum.GetName(typeof(ConsoleKey), Controls.MoveLeftAlternative).SplitByCapitalLetters()},
            {"$RIGHT1", Enum.GetName(typeof(ConsoleKey), Controls.MoveRight).SplitByCapitalLetters()},
            {"$RIGHT2", Enum.GetName(typeof(ConsoleKey), Controls.MoveRightAlternative).SplitByCapitalLetters()},
            {"$UP1", Enum.GetName(typeof(ConsoleKey), Controls.MoveUp).SplitByCapitalLetters()},
            {"$UP2", Enum.GetName(typeof(ConsoleKey), Controls.MoveUpAlternative).SplitByCapitalLetters()},
            {"$DOWN1", Enum.GetName(typeof(ConsoleKey), Controls.MoveDown).SplitByCapitalLetters()},
            {"$DOWN2", Enum.GetName(typeof(ConsoleKey), Controls.MoveDownAlternative).SplitByCapitalLetters()},

            {"$INVENTORYEXIT", Enum.GetName(typeof(ConsoleKey), Controls.InventoryExitKey).SplitByCapitalLetters()},
            {"$INVENTORYSPECS", Enum.GetName(typeof(ConsoleKey), Controls.InventorySpecificationsKey).SplitByCapitalLetters()},
            {"$INVENTORYEQUIP", Enum.GetName(typeof(ConsoleKey), Controls.InventoryEquipUseKey).SplitByCapitalLetters()},
            {"$INVENTORY", Enum.GetName(typeof(ConsoleKey), Controls.InventoryKey).SplitByCapitalLetters()},
        };

        public static string ReplaceKeywords(this string source) {
            string newstr = source;

            foreach (var set in _replacementStrings) {
                newstr = newstr.Replace(set.Key, set.Value);
            }

            return newstr;
        }

        /// <summary>
        /// Take a string and split it by the capital letters in the string with spaces in between each capitalized word. e.g. ThisWordIs => This Word Is
        /// </summary>
        /// <param name="source">The string to change</param>
        /// <returns>The newly formatted string</returns>
        public static string SplitByCapitalLetters(this string source) {
            string[] words = System.Text.RegularExpressions.Regex.Split(source, @"(?<!^)(?=[A-Z])");

            return words.Aggregate((str1, str2) => (str1 + " " + str2));
        }

        /// <summary>
        /// Wraps a string into the current console window
        /// </summary>
        /// <param name="oldString">The string to wrap</param>
        /// <param name="setPositionToZero">Whether to move the cursor to position 0,0 first (default=true)</param>
        /// <returns>The newly formatted string</returns>
        public static string WordWrap(this string oldString, bool setPositionToZero = true) {
            string currString = "";
            string newString = "";
            string[] words = oldString.Split(new char[] { ' ' });

            if (setPositionToZero) {
                Console.SetCursorPosition(0, 0);
            }

            foreach (var item in words) {
                if (String.IsNullOrEmpty(item)) {
                    continue;
                }

                if ((currString + item + " ").Length < Console.BufferWidth - 1) {
                    currString += item + " ";
                } else {
                    newString += currString + "\n";
                    currString = item + " ";
                }
            }

            return newString + currString;
        }


        /// <summary>
        /// Wraps a string into the current console window
        /// </summary>
        /// <param name="oldString">The string to wrap</param>
        /// <param name="width">The width to wrap around</param>
        /// <param name="setPositionToZero">Whether to move the cursor to position 0,0 first (default=true)</param>
        /// <returns>The newly formatted string</returns>
        public static string WordWrap(this string oldString, int width, bool setPositionToZero = true) {
            string currString = "";
            string newString = "";
            string[] words = oldString.Split(new char[] { ' ' });

            if (setPositionToZero) {
                Console.SetCursorPosition(0, 0);
            }

            foreach (var item in words) {
                if (String.IsNullOrEmpty(item)) {
                    continue;
                }

                if ((currString + item + " ").Length < width - 1) {
                    currString += item + " ";
                } else {
                    newString += currString + "\n";
                    currString = item + " ";
                }
            }

            return newString + currString;
        }

        /// <summary>
        /// Write a string to the console in the given color (will use DefaultConsoleColor if ColorsEnabled == false)
        /// </summary>
        /// <param name="str">The string to write</param>
        /// <param name="color">The color to print the string in</param>
        public static void WriteToConsole(this string str, Color color) {
            if (GameVariables.ColorsEnabled) {
                Console.Write(str, color);
            } else {
                Console.Write(str, ItemColors.DefaultConsoleColor);
            }

            // Go back to the default color after writing the string out
            Console.ForegroundColor = ItemColors.DefaultConsoleColor;
        }

        /// <summary>
        /// Write a char to the console in the given color (will use DefaultConsoleColor if ColorsEnabled == false)
        /// </summary>
        /// <param name="str">The char to write</param>
        /// <param name="color">The color to print the char in</param>
        public static void WriteToConsole(this char ch, Color color) {
            if (GameVariables.ColorsEnabled) {
                Console.Write(ch, color);
            } else {
                Console.Write(ch, ItemColors.DefaultConsoleColor);
            }

            // Go back to the default color after writing the char out
            Console.ForegroundColor = ItemColors.DefaultConsoleColor;
        }

        /// <summary>
        /// Write a string to the console using the defualt color
        /// </summary>
        /// <param name="str">The string to write</param>
        public static void WriteToConsole(this string str) {
            WriteToConsole(str, ItemColors.DefaultConsoleColor);
        }

        /// <summary>
        /// Write a char to the console using the defualt color
        /// </summary>
        /// <param name="str">The char to write</param>
        public static void WriteToConsole(this char ch) {
            WriteToConsole(ch, ItemColors.DefaultConsoleColor);
        }

        /// <summary>
        /// Get a random element from the list of things
        /// </summary>
        /// <typeparam name="T">The type for the list</typeparam>
        /// <param name="itemList">The list of things</param>
        /// <returns>The random element chosen</returns>
        public static T GetRandomElement<T>(this List<T> itemList) {
            int choice = GameVariables.Randomizer.Next(0, itemList.Count);
            return itemList[choice];
        }
    }
}
