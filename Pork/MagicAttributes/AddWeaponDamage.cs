﻿using System;

namespace Pork {
    class AddWeaponDamage : IMagicAttribute {
        public Action ActivateAttribute { private set; get; }
        public Action DeactivateAttribute { private set; get; }
        public bool Passive { private set; get; }
        public string MagicNameExtension { private set; get; }
        public string Description { private set; get; }

        private int _damageAmount;

        /// <summary>
        /// Additional weapon damage attribute
        /// </summary>
        /// <param name="nameExtension">The name of the attribute extension</param>
        /// <param name="amount">How much damage it adds</param>
        public AddWeaponDamage(string nameExtension, int amount) {
            MagicNameExtension = nameExtension;
            Passive = true;
            _damageAmount = amount;
            Description = string.Format("+{0} damage", _damageAmount);
        }

        /// <summary>
        /// Set the owner player for this attribute
        /// </summary>
        /// <param name="owner">The owner object to set this to</param>
        public void SetPlayerOwner(IPlayer owner) {
            ActivateAttribute = (() => owner.CurrentWeapon.WeaponDamage += _damageAmount);
            DeactivateAttribute = (() => owner.CurrentWeapon.WeaponDamage -= _damageAmount);
        }
    }
}
