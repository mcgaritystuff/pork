﻿using System;

namespace Pork {
    class AddArmorPoints : IMagicAttribute {
        public Action ActivateAttribute { private set; get; }
        public Action DeactivateAttribute { private set; get; }
        public bool Passive { private set; get; }
        public string MagicNameExtension { private set; get; }
        public string Description { private set; get; }

        private Armor _armor;
        private int _armorPoints;

        /// <summary>
        /// Add player armor points attribute
        /// </summary>
        /// <param name="nameExtension">The name of the attribute extension</param>
        /// <param name="amount">How many armor points it adds</param>
        public AddArmorPoints(string nameExtension, int amount, Armor armor) {
            MagicNameExtension = nameExtension;
            Passive = true;
            _armorPoints = amount;
            _armor = armor;
            Description = string.Format("+{0} to armor", _armorPoints);
        }

        /// <summary>
        /// Set the owner player for this attribute
        /// </summary>
        /// <param name="owner">The owner object to set this to</param>
        public void SetPlayerOwner(IPlayer owner) {
            ActivateAttribute = (() => _armor.ArmorPoints += _armorPoints);
            DeactivateAttribute = (() => _armor.ArmorPoints -= _armorPoints);
        }
    }
}
