﻿using System;

namespace Pork {
    class AddPlayerHealth : IMagicAttribute {
        public Action ActivateAttribute { private set; get; }
        public Action DeactivateAttribute { private set; get; }
        public bool Passive { private set; get; }
        public string MagicNameExtension { private set; get; }
        public string Description { private set; get; }

        private int _healthAmount;

        /// <summary>
        /// Add player health attribute
        /// </summary>
        /// <param name="nameExtension">The name of the attribute extension</param>
        /// <param name="amount">How much health it adds</param>
        public AddPlayerHealth(string nameExtension, int amount) {
            MagicNameExtension = nameExtension;
            Passive = true;
            _healthAmount = amount;
            Description = string.Format("+{0} to max player health", _healthAmount);
        }

        /// <summary>
        /// Set the owner player for this attribute
        /// </summary>
        /// <param name="owner">The owner object to set this to</param>
        public void SetPlayerOwner(IPlayer owner) {
            ActivateAttribute = (() => owner.MaxHealth += _healthAmount);
            DeactivateAttribute = (() => owner.MaxHealth -= _healthAmount);
        }
    }
}
