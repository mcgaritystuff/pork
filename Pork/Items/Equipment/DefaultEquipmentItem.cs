﻿using System.Collections.Generic;
using System.Linq;

namespace Pork {
    class DefaultEquipmentItem : GameObject, IEquippable {
        public List<IMagicAttribute> MagicAttributes { protected set; get; }
        public bool IsEquipped { protected set; get; }
        public string EquipmentDescription { protected set; get; }
        public string EquipmentName { protected set; get; }

        /// <summary>
        /// The core equipment item for things like weapons, armor, etc.
        /// </summary>
        /// <param name="attribs">Magical attributes list</param>
        /// <param name="equipmentDescription">Description of the item</param>
        /// <param name="equipmentName">Textual name of the item</param>
        /// <param name="position">Where it is if on the map</param>
        /// <param name="icon">Icon if displayed on the map</param>
        public DefaultEquipmentItem(List<IMagicAttribute> attribs, string equipmentDescription, string equipmentName, Point position, char icon) {
            MagicAttributes = attribs;
            EquipmentDescription = equipmentDescription;
            EquipmentName = equipmentName;
            Position = position;
            Icon = icon;
            ObjectColor = ItemColors.Equipment;

            // If there's a magical attribute, rename the item to the first attribute in the list + the equipmentName
            if (MagicAttributes != null) {
                var firstAttrib = MagicAttributes.FirstOrDefault(attrib => !string.IsNullOrEmpty(attrib.MagicNameExtension));
                EquipmentName = string.Format("{0} {1}", firstAttrib.MagicNameExtension, EquipmentName);
            }
        }

        /// <summary>
        /// Personal update method for the item
        /// </summary>
        public override void Update() {
            //Honestly, there's nothing to update if it's not drawn.
            if (!DrawOnMap) {
                return;
            }

            base.Update();

            // Validate whether the item is being given to the player. If it's a weapon, add it to the weapon's inventory list
            GameVariables.GameObjectsList.ForEach(obj => {
                if (obj is Player && (obj as Player).Position.Equals(this.Position)) {
                    var _playerOwner = obj as IPlayer;
                    DrawOnMap = false;
                    if (this is IWeapon) {
                        _playerOwner.PlayerInventory.AddWeapon(this as IWeapon);
                    } else {
                        _playerOwner.PlayerInventory.AddEquippableItem(this);
                    }
                }
            });
        }

        /// <summary>
        /// Equip an item to the player
        /// </summary>
        /// <param name="owner">The player who owns the item</param>
        public void EquipItem(IPlayer owner) {
            IsEquipped = true;

            if (MagicAttributes == null) {
                return;
            }

            // Activate passive magic attributes if applicable
            MagicAttributes.ForEach(attrib => {
                attrib.SetPlayerOwner(owner);
                if (attrib.Passive) {
                    attrib.ActivateAttribute();
                }
            });
        }

        /// <summary>
        /// Hide the item from the map drawing
        /// </summary>
        public void Hide() {
            this.DrawOnMap = false;
        }

        /// <summary>
        /// Unequip the item from the player
        /// </summary>
        /// <param name="owner">The player who owns the item</param>
        public void UnequipItem(IPlayer owner) {
            IsEquipped = false;

            if (MagicAttributes == null) {
                return;
            }

            // Deactivate any passive magic attributes if applicable
            MagicAttributes.ForEach(attrib => {
                if (attrib.Passive) {
                    attrib.DeactivateAttribute();
                }
            });
        }
    }
}
