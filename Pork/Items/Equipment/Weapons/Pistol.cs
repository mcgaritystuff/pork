﻿using System.Collections.Generic;

namespace Pork {
    class Pistol : DefaultEquipmentItem, IWeapon {
        public uint CurrentShootTimer { private set; get; }
        public uint MaxShootTimer { private set; get; }
        public int BulletSpeed { set; get; }
        public int WeaponDamage { set; get; }

        /// <summary>
        /// Create a pistol for the player
        /// </summary>
        /// <param name="maxShootTime">How often this weapon should be able to shoot (lower = faster shoot rate)</param>
        /// <param name="bulletSpeed">How fast the bullet travels</param>
        /// <param name="weaponDamage">How much damage the bullets do</param>
        /// <param name="attributes">Special magic attributes the gun has</param>
        /// <param name="position">Position if on the map</param>
        /// <param name="icon">Icon for the gun if it's on the map</param>
        public Pistol(uint maxShootTime, int bulletSpeed, int weaponDamage, List<IMagicAttribute> attributes, Point position, char icon)
            : base(attributes, "Shoots 1 bullet", "Pistol", position, icon) {
            MaxShootTimer = maxShootTime;
            BulletSpeed = bulletSpeed;
            WeaponDamage = weaponDamage;

            CurrentShootTimer = 0;
        }

        /// <summary>
        /// Personal update method to adjust current shoot timer
        /// </summary>
        public override void Update() {
            CurrentShootTimer++;
        }

        /// <summary>
        /// Shoot the gun!
        /// </summary>
        /// <param name="direction">Which way the bullet should be going</param>
        /// <param name="position">Starting position for the bullet</param>
        /// <param name="enemyBullet">Whether this bullet is an enemy's or not</param>
        public void Shoot(Point direction, Point position, bool enemyBullet) {
            if (CurrentShootTimer >= MaxShootTimer) {
                new Bullet(direction, BulletSpeed, position, enemyBullet, WeaponDamage);
                GlobalMethods.PlaySound("Pistol.wav");
                CurrentShootTimer = 0;
            }
        }
    }
}
