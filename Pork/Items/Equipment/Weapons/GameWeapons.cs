﻿using System.Collections.Generic;

namespace Pork {
    static class GameWeapons {
        private static readonly char _weaponIcon = '☼';

        /// <summary>
        /// A normal pistol, nothing special
        /// </summary>
        /// <param name="position">Where it should be placed</param>
        /// <returns>The pistol</returns>
        public static Pistol PistolNormal(Point position) {
            return new Pistol(30, 10, 5, null, position, _weaponIcon);
        }

        /// <summary>
        /// A normal shotgun, nothing special
        /// </summary>
        /// <param name="position">Where it should be placed</param>
        /// <returns>The shotgun</returns>
        public static Shotgun ShotgunNormal(Point position) {
            return new Shotgun(30, 10, 5, null, position, _weaponIcon);
        }

        /// <summary>
        /// A strong shotgun
        /// </summary>
        /// <param name="position">Where it should be placed</param>
        /// <returns>The shotgun</returns>
        public static Shotgun ShotgunStrong(Point position) {
            List<IMagicAttribute> strongShotgunAttribute = new List<IMagicAttribute>() { new AddWeaponDamage("Strong", 5) };
            return new Shotgun(30, 10, 5, strongShotgunAttribute, position, _weaponIcon);
        }
    }
}
