﻿using System;
using System.Linq;

namespace Pork {
    public class Bullet : GameObject {
        private readonly Point _direction;

        private readonly bool _enemyBullet;
        private readonly int _bulletSpeed;
        private readonly int _damage;
        private readonly uint _maxTimer;

        private uint _timer;

        /// <summary>
        /// Create a bullet
        /// </summary>
        /// <param name="direction">The direction the bullet is moving</param>
        /// <param name="bulletSpeed">How fast it's moving</param>
        /// <param name="startPosition">Where it's spawned</param>
        /// <param name="enemyBullet">True if this should hurt the player, false if it should hurt an enemy</param>
        /// <param name="damage">How much damage it should inflict</param>
        public Bullet(Point direction, int bulletSpeed,
                      Point startPosition, bool enemyBullet, int damage) {
            if (startPosition.X < 0 || startPosition.X > Console.BufferWidth - 1 ||
                startPosition.Y < 0 || startPosition.Y > Console.BufferHeight - 1 ||
                GlobalMethods.CheckForSolidCollisions(startPosition) ||
                GlobalMethods.CheckForGameObjects(startPosition) is ExitArea) {
                Position = new Point(-1, -1);
                WaitingRemoval = true;
                return;
            }
            _direction = direction;
            _bulletSpeed = bulletSpeed;
            Position = startPosition;
            _enemyBullet = enemyBullet;
            _damage = damage;

            _timer = 0;
            _maxTimer = (uint)(GameVariables.FrameRate / bulletSpeed);
            Icon = '*';
            ObjectColor = ItemColors.Bullet;
        }

        /// <summary>
        /// Update the bullet and destroy if it his something
        /// </summary>
        public override void Update() {
            base.Update();

            if (WaitingRemoval) {
                return;
            }

            if (++_timer >= _maxTimer) {
                _timer = 0;

                // Check collisions or update the position if the bullet is still in the screen. If it's
                // not, then destroy it
                if (Position.X + _direction.X >= 0 &&
                    Position.X + _direction.X <= Console.BufferWidth - 1 &&
                    Position.Y + _direction.Y >= 0 &&
                    Position.Y + _direction.Y <= Console.BufferHeight - 1) {
                    Point newPosition = new Point(Position.X + _direction.X, Position.Y + _direction.Y);
                    if (GlobalMethods.CheckForSolidCollisions(newPosition) ||
                        GlobalMethods.CheckForGameObjects(newPosition) is ExitArea) {
                        WaitingRemoval = true;
                        return;
                    } else
                        Position = newPosition;
                } else {
                    WaitingRemoval = true;
                    return;
                }
            }
            
            // Finish it off w/ checking for creature collisions
            CheckForCreatureCollisions();
        }

        /// <summary>
        /// Check for creatures and hurt them accordingly
        /// </summary>
        /// <returns>True if it hit something, false otherwise</returns>
        private bool CheckForCreatureCollisions() {
            if (_enemyBullet) {
                foreach (var player in GameVariables.GameObjectsList.Where(item => item is IPlayer)) {
                    if (player.Position.Equals(Position)) {
                        (player as IPlayer).TakeDamage(_damage);
                        WaitingRemoval = true;
                        return true;
                    }
                }
            } else {
                foreach (var enemy in GameVariables.GameObjectsList.Where(item => item is IEnemy)) {
                    if (enemy.Position.Equals(Position)) {
                        (enemy as IEnemy).TakeDamage(_damage);
                        WaitingRemoval = true;
                        return true;
                    }
                }
            }

            return false;
        }
    }
}
