﻿using System.Collections.Generic;

namespace Pork {
    class Shotgun : DefaultEquipmentItem, IWeapon {
        public uint CurrentShootTimer { private set; get; }
        public uint MaxShootTimer { private set; get; }
        public int BulletSpeed { set; get; }
        public int WeaponDamage { set; get; }

        /// <summary>
        /// Create a shotgun for the player
        /// </summary>
        /// <param name="maxShootTime">How often this weapon should be able to shoot (lower = faster shoot rate)</param>
        /// <param name="bulletSpeed">How fast the bullets travel</param>
        /// <param name="weaponDamage">How much damage the bullets do</param>
        /// <param name="attributes">Special magic attributes the gun has</param>
        /// <param name="position">Position if on the map</param>
        /// <param name="icon">Icon for the gun if it's on the map</param>
        public Shotgun(uint maxShootTime, int bulletSpeed, int weaponDamage, List<IMagicAttribute> attributes, Point position, char icon)
            : base(attributes, "Shoots multiple bullets", "Shotgun", position, icon) {
            MaxShootTimer = maxShootTime;
            BulletSpeed = bulletSpeed;
            WeaponDamage = weaponDamage;

            CurrentShootTimer = 0;
        }

        /// <summary>
        /// Personal update method to adjust current shoot timer
        /// </summary>
        public override void Update() {
            CurrentShootTimer++;
        }

        /// <summary>
        /// Shoot the gun!
        /// </summary>
        /// <param name="direction">Which way the bullets should be going</param>
        /// <param name="position">Starting position for the bullet</param>
        /// <param name="enemyBullet">Whether this bullets are an enemy's or not</param>
        public void Shoot(Point direction, Point position, bool enemyBullet) {
            if (CurrentShootTimer >= MaxShootTimer) {
                new Bullet(direction, BulletSpeed, position, enemyBullet, WeaponDamage);
                if (direction.X == 0) {
                    new Bullet(new Point(1, direction.Y), BulletSpeed, position, enemyBullet, WeaponDamage);
                    new Bullet(new Point(-1, direction.Y), BulletSpeed, position, enemyBullet, WeaponDamage);
                } else {
                    new Bullet(new Point(direction.X, 1), BulletSpeed, position, enemyBullet, WeaponDamage);
                    new Bullet(new Point(direction.X, -1), BulletSpeed, position, enemyBullet, WeaponDamage);
                }
                GlobalMethods.PlaySound("Shotgun.wav");
                CurrentShootTimer = 0;
            }
        }
    }
}
