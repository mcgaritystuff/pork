﻿using System.Collections.Generic;

namespace Pork {
    static class GameArmors {
        private static readonly char _armorIcon = '☼';

        /// <summary>
        /// Normal cow hide armor
        /// </summary>
        /// <param name="position">Where to place the armor</param>
        /// <returns>The armor</returns>
        public static Armor CowHideNormal(Point position) {
            return new Armor("Cow Hide", 2, "Some tough armor", null, position, _armorIcon);
        }

        /// <summary>
        /// Cow hide armor with "add player health" attribute
        /// </summary>
        /// <param name="health">How much health to give</param>
        /// <param name="position">Where to place the armor</param>
        /// <returns>The armor</returns>
        public static Armor CowHideHealthy(int health, Point position) {
            List<IMagicAttribute> healthyArmorAttributes = new List<IMagicAttribute>() { new AddPlayerHealth("Healthy", health) };
            return new Armor("Cow Hide", 2, "Some tough armor", healthyArmorAttributes, position, _armorIcon);
        }

        /// <summary>
        /// Godly cow armor (has player health + armor points)
        /// </summary>
        /// <param name="addArmor">How many armor points</param>
        /// <param name="health">How much health to give</param>
        /// <param name="position">Where to place the armor</param>
        /// <returns>The armor</returns>
        public static Armor CowHideGodly(int addArmor, int health, Point position) {
            Armor armorTemplate = CowHideNormal(position);
            List<IMagicAttribute> godlyArmorAttributes = new List<IMagicAttribute>()
            {
                new AddPlayerHealth("Godly", health),
                new AddArmorPoints("Blah", addArmor, armorTemplate)
            };
            Armor godArmor = new Armor(armorTemplate.EquipmentName, 2 + addArmor, armorTemplate.EquipmentDescription, godlyArmorAttributes, position, _armorIcon);
            GameVariables.GameObjectsList.RemoveAll(obj => obj.Equals(armorTemplate)); //Cause it gets added to the list when we create it. We only needed it for text information
            return godArmor;
        }
    }
}
