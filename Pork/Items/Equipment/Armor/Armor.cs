﻿using System.Collections.Generic;

namespace Pork {
    class Armor : DefaultEquipmentItem, IEquippable {
        public int ArmorPoints { set; get; }

        /// <summary>
        /// Create some armor for the player!
        /// </summary>
        /// <param name="name">The name of the armor</param>
        /// <param name="armorPoints">How many armor points it gives</param>
        /// <param name="description">Description of the armor</param>
        /// <param name="attributes">Any magical attributes</param>
        /// <param name="position">Where it is on the map (if not inventory)</param>
        /// <param name="icon">The icon to display it as (on the map)</param>
        public Armor(string name, int armorPoints, string description, List<IMagicAttribute> attributes, Point position, char icon)
            : base(attributes, description, name, position, icon) {
            ArmorPoints = armorPoints;
        }
    }
}
