﻿namespace Pork {
    static class GameItems {
        private static char _potionIcon = '☼';

        /// <summary>
        /// Create a small health potion
        /// </summary>
        /// <param name="position">Position on the map</param>
        /// <returns>A small health potion item</returns>
        public static Potion SmallHealPotion(Point position) {
            return new Potion("Sm Heal Potion", 5, position, _potionIcon);
        }

        /// <summary>
        /// Create a medium health potion
        /// </summary>
        /// <param name="position">Position on the map</param>
        /// <returns>A medium health potion item</returns>
        public static Potion MediumHealPotion(Point position) {
            return new Potion("Med Heal Potion", 15, position, _potionIcon);
        }

        /// <summary>
        /// Create a large health potion
        /// </summary>
        /// <param name="position">Position on the map</param>
        /// <returns>A large health potion item</returns>
        public static Potion LargeHealPotion(Point position) {
            return new Potion("Lg Heal Potion", 30, position, _potionIcon);
        }

        /// <summary>
        /// Create a extra large health potion
        /// </summary>
        /// <param name="position">Position on the map</param>
        /// <returns>A extra large health potion item</returns>
        public static Potion ExtraLargeHealPotion(Point position) {
            return new Potion("XL Heal Potion", 50, position, _potionIcon);
        }

        /// <summary>
        /// Creates currency
        /// </summary>
        /// <param name="value">The amount of currency the player should get</param>
        /// <param name="position">Position on the map</param>
        /// <returns>Animal feed!</returns>
        public static Currency AnimFeed(uint value, Point position) {
            return new Currency(ItemColors.Currency, '$', value, position);
        }
    }
}
