﻿namespace Pork {
    class Potion : GameObject, IItem {
        public string ItemDescription { private set; get; }
        public string ItemName { private set; get; }
        public int HealAmount { private set; get; }

        private IPlayer _playerOwner;

        /// <summary>
        /// Create a potion item to heal the player w/
        /// </summary>
        /// <param name="name">The name of the potion</param>
        /// <param name="healAmount">How much to heal the player</param>
        /// <param name="position">Where to draw if on the map</param>
        /// <param name="icon">The icon if on the map</param>
        public Potion(string name, int healAmount, Point position, char icon) {
            ItemName = name;
            HealAmount = healAmount;
            Position = position;
            Icon = icon;
            ObjectColor = ItemColors.Potion;
            ItemDescription = string.Format("Heals {0} HP", HealAmount);
            _alwaysDrawObject = true;
        }

        /// <summary>
        /// Personal update method to update if on the map
        /// </summary>
        public override void Update() {
            //Honestly, there's nothing to update if it's not drawn.
            if (!DrawOnMap) {
                return;
            }

            base.Update();

            // Check collisions w/ the player to give them the potion
            GameVariables.GameObjectsList.ForEach(obj => {
                if (obj is Player && (obj as Player).Position.Equals(this.Position)) {
                    this._playerOwner = obj as IPlayer;
                    DrawOnMap = false;
                    this._playerOwner.PlayerInventory.AddItem(this);
                }
            });
        }

        /// <summary>
        /// Draw the potion on the map
        /// </summary>
        public override void Draw() {
            base.Draw();
        }

        /// <summary>
        /// Hide the potion from the map
        /// </summary>
        public void Hide() {
            this.DrawOnMap = false;
        }

        /// <summary>
        /// Heal the player
        /// </summary>
        public void UseItem() {
            _playerOwner.Heal(HealAmount);
            WaitingRemoval = true;
        }
    }
}
