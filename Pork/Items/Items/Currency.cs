﻿using System.Drawing;
using System.Linq;

namespace Pork {
    public class Currency : GameObject {
        public uint Value { private set; get; }

        /// <summary>
        /// Create an currency object for currency (only used for the map)
        /// </summary>
        /// <param name="color">The color of the currency</param>
        /// <param name="icon">The icon to draw on the map</param>
        /// <param name="value">How much currency</param>
        /// <param name="position">Where to draw on the map</param>
        public Currency(Color color, char icon, uint value, Point position) {
            this.ObjectColor = color;
            this.Icon = icon;
            this.Value = value;
            this.Position = position;
            _alwaysDrawObject = true;
        }

        /// <summary>
        /// Basically just check against collisions w/ the player
        /// </summary>
        public override void Update() {
            if (WaitingRemoval) {
                return;
            }

            base.Update();

            // Pay up the player if they collide w/ it
            foreach (var player in GameVariables.GameObjectsList.Where(obj => obj is IPlayer)) {
                if (this.Position.Equals(player.Position)) {
                    (player as IPlayer).GainCurrency(this.Value);
                    WaitingRemoval = true;
                    return;
                }
            }
        }

        /// <summary>
        /// Draw the currency
        /// </summary>
        public override void Draw() {
            if (WaitingRemoval) {
                return;
            }

            base.Draw();
        }

    }
}
