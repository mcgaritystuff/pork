﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Pork {
    class NormalEnemy : GameObject, IEnemy {
        public int Damage { private set; get; }
        public int Health { private set; get; }
        public int MaxHealth { set; get; }
        public uint MoveSpeed { private set; get; }
        public IWeapon CurrentWeapon { private set; get; }
        public Point CreatureDirection { private set; get; }

        private uint _moveTimer;
        private readonly uint _maxMoveTimer;

        /// <summary>
        /// Create a normal enemy (nothing special about it, just moves around)
        /// </summary>
        /// <param name="startPosition">Where it's spawned at</param>
        /// <param name="icon">The icon to use</param>
        /// <param name="color">The color of the enemy</param>
        /// <param name="moveSpeed">How fast it moves on the screen</param>
        /// <param name="damage">How much damage if it hits the player</param>
        public NormalEnemy(Point startPosition, char icon, Color color, uint moveSpeed, int damage) {
            Position = startPosition;
            Icon = icon;
            ObjectColor = color;
            MoveSpeed = moveSpeed;
            Damage = damage;

            _moveTimer = 0;
            _maxMoveTimer = (uint)(GameVariables.FrameRate / MoveSpeed);

            Health = MaxHealth = 5;
        }

        /// <summary>
        /// Update movement and such
        /// </summary>
        public override void Update() {
            base.Update();

            if (WaitingRemoval) {
                return;
            }

            if (++_moveTimer >= _maxMoveTimer) {
                Move();
            }

            CheckPlayerCollision();
        }

        /// <summary>
        /// Heal the enemy
        /// </summary>
        /// <param name="health">How much to heal</param>
        public void Heal(int health) {
            Health += health;
        }

        /// <summary>
        /// Take damage!
        /// </summary>
        /// <param name="damage">The damage to take</param>
        public void TakeDamage(int damage) {
            Health -= damage;
            if (Health <= 0) {
                WaitingRemoval = true;
            }
        }

        /// <summary>
        /// Destroy the enemy object and drop a random item and/or currency
        /// </summary>
        public override void Destroy() {
            base.Destroy();

            List<Point> availableSpots = new List<Point>();

            // Check all the available spots around the enemy before destroying it
            for (int x = this.Position.X - 1; x <= this.Position.X + 1; x++) {
                for (int y = this.Position.Y - 1; y <= this.Position.Y + 1; y++) {
                    Point spot = new Point(x, y);
                    if (GameVariables.LevelBorder.InsideBox(ref spot) && !GlobalMethods.CheckForSolidCollisions(spot) &&
                        !spot.Equals(this.Position)) {
                        availableSpots.Add(spot);
                    }
                }
            }

            // TODO: put these somewhere else
            // number / 100 % chance of dropping
            int chanceOfFeed = 30;
            int chanceOfPotion = 10;

            #region Currency
            if (availableSpots.Count < 1) {
                return;
            }
            int chance = GameVariables.Randomizer.Next(0, 100);
            if (chance <= chanceOfFeed) {
                Point spot = availableSpots.GetRandomElement();
                GameItems.AnimFeed((uint)GameVariables.Randomizer.Next(1, 25), spot);
                availableSpots.RemoveAll(p => p.Equals(spot));
            }
            #endregion

            #region item(s)
            if (availableSpots.Count < 1) {
                return;
            }

            chance = GameVariables.Randomizer.Next(0, 100);
            if (chance <= chanceOfPotion) {
                Point spot = availableSpots.GetRandomElement();
                GameItems.SmallHealPotion(spot);
                availableSpots.RemoveAll(p => p.Equals(spot));
            }
            #endregion
        }

        /// <summary>
        /// Move the enemy somewhere on the map (in north, east, south, or west directions)
        /// </summary>
        private void Move() {
            _moveTimer = 0;
            byte newDirection = (byte)GameVariables.Randomizer.Next(Direction.NumberOfDirections / 4); // 4 in case there's more than 4 directions (NESW)
            Point newPosition = Position;

            switch (newDirection) {
                case 0://North
                    if (Position.Y > 0) {
                        newPosition = new Point(Position.X, Position.Y - 1);
                    }
                    if (!GlobalMethods.CheckForSolidCollisions(newPosition) && !(GlobalMethods.CheckForGameObjects(newPosition) is ExitArea)) {
                        Position = newPosition;
                    }
                    break;
                case 1: //South
                    if (Position.Y < Console.BufferHeight - 1) {
                        newPosition = new Point(Position.X, Position.Y + 1);
                    }
                    if (!GlobalMethods.CheckForSolidCollisions(newPosition) && !(GlobalMethods.CheckForGameObjects(newPosition) is ExitArea)) {
                        Position = newPosition;
                    }
                    break;
                case 3: //West
                    if (Position.X > 0) {
                        newPosition = new Point(Position.X - 1, Position.Y);
                    }
                    if (!GlobalMethods.CheckForSolidCollisions(newPosition) && !(GlobalMethods.CheckForGameObjects(newPosition) is ExitArea)) {
                        Position = newPosition;
                    }
                    break;
                case 2: //East
                    if (Position.X < Console.BufferWidth - 1) {
                        newPosition = new Point(Position.X + 1, Position.Y);
                    }
                    if (!GlobalMethods.CheckForSolidCollisions(newPosition) && !(GlobalMethods.CheckForGameObjects(newPosition) is ExitArea)) {
                        Position = newPosition;
                    }
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// As it sounds, check for collisions with the player
        /// </summary>
        private void CheckPlayerCollision() {
            foreach (var player in GameVariables.GameObjectsList.Where(item => item is IPlayer)) {
                if (player.Position.X == this.Position.X &&
                    player.Position.Y == this.Position.Y) {
                    (player as IPlayer).TakeDamage(Damage);
                    WaitingRemoval = true;
                }
            }
        }
    }
}
