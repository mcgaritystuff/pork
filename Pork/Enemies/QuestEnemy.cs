﻿using System.Drawing;

namespace Pork {
    class QuestEnemy : NormalEnemy {
        private bool _showAtChapter;
        private int _chapter;

        /// <summary>
        /// Creates an enemy tied to a quest
        /// </summary>
        /// <param name="startPosition">Where to spawn</param>
        /// <param name="icon">The icon to draw the enemy w/</param>
        /// <param name="color">The color of the enemy</param>
        /// <param name="moveSpeed">How fast the enemy moves</param>
        /// <param name="damage">Damage if it hits the player</param>
        /// <param name="chapter">The chapter this enemy should be at</param>
        /// <param name="showAtChapter">Whether the enemy shows (true) or is destroyed (false) at the chapter</param>
        public QuestEnemy(Point startPosition, char icon, Color color, uint moveSpeed, int damage, int chapter, bool showAtChapter)
            : base(startPosition, icon, color, moveSpeed, damage) {
            _showAtChapter = showAtChapter;
            _chapter = chapter;

            GameVariables.IncrementCurrentChapter += (() => Enabled = GlobalMethods.ContinueUpdateOrDraw(this, _showAtChapter, _chapter));
            Enabled = GlobalMethods.ContinueUpdateOrDraw(this, _showAtChapter, _chapter);
        }
    }
}
