﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;

namespace Pork {

    /// <summary>
    /// A point on the screen of the console window
    /// </summary>
    public struct Point : IEquatable<Point> {
        public int X { get; private set; }
        public int Y { get; private set; }

        public static Point Zero = new Point(0, 0);

        /// <summary>
        /// Simply a point on the console window
        /// </summary>
        /// <param name="x">X-axis coordinate</param>
        /// <param name="y">Y-axis coordinate</param>
        public Point(int x, int y) : this() {
            this.X = x;
            this.Y = y;
        }

        /// <summary>
        /// Check to see if the Point is equal to another point
        /// </summary>
        /// <param name="otherPoint">The other point being compared to</param>
        /// <returns>True if they're equal, false otherwise</returns>
        public override bool Equals(object otherPoint) {
            // Only compare to points
            if (!(otherPoint is Point)) {
                return false;
            }
            // Return whether they're equal using our custom method that takes a Point object
            return Equals((Point)otherPoint);

        }

        /// <summary>
        /// Check to see if the Point is equal to another point
        /// </summary>
        /// <param name="otherPoint">The other point being compared to</param>
        /// <returns>True if they're equal, false otherwise</returns>
        public bool Equals(Point otherPoint) {
            return this.X == otherPoint.X && this.Y == otherPoint.Y;
        }

        /// <summary>
        /// Normal C# standards
        /// </summary>
        /// <returns>X * 31 + Y</returns>
        public override int GetHashCode() {
            return X * 31 + Y;
        }

        /// <summary>
        /// Add two Point coordinates
        /// </summary>
        /// <param name="p1">Point 1</param>
        /// <param name="p2">Point 2</param>
        /// <returns>Points added together (p1.X + p2.X, p1.Y + p2.Y)</returns>
        public static Point operator +(Point p1, Point p2) {
            return new Point(p1.X + p2.X, p1.Y + p2.Y);
        }

        /// <summary>
        /// Subtract two Point coordinates
        /// </summary>
        /// <param name="p1">Point 1</param>
        /// <param name="p2">Point 2</param>
        /// <returns>Points subtracted together (p1.X - p2.X, p1.Y - p2.Y)</returns>
        public static Point operator -(Point p1, Point p2) {
            return new Point(p1.X - p2.X, p1.Y - p2.Y);
        }
    }

    /// <summary>
    /// A box, which can be used for barriers, the screen, whatever and can be used
    /// to determine if a point is inside of a given location (to keep it from 
    /// escaping/entering the location)
    /// </summary>
    public struct Box {
        public int Left { private set; get; }
        public int Right { private set; get; }
        public int Top { private set; get; }
        public int Bottom { private set; get; }

        /// <summary>
        /// The box to create
        /// </summary>
        /// <param name="left">Left side of the box (i.e. X = 0)</param>
        /// <param name="right">Right side of the box (i.e. X = 20)</param>
        /// <param name="top">Top side of the box (i.e. Y = 0)</param>
        /// <param name="bottom">Bottom side of the box (i.e. Y = 10)</param>
        public Box(int left, int right, int top, int bottom) : this() {
            this.Left = left;
            this.Right = right;
            this.Top = top;
            this.Bottom = bottom;
        }

        /// <summary>
        /// To determine if a given point is within the box
        /// </summary>
        /// <param name="otherPoint">The point to check against</param>
        /// <returns>True if it's within the box barrier, false otherwise</returns>
        public bool InsideBox(ref Point otherPoint) {

            if (otherPoint.X > Left && 
                otherPoint.X < Right &&
                otherPoint.Y < Bottom && 
                otherPoint.Y > Top) {
                return true;
            }

            return false;
        }
    }

    /// <summary>
    /// A structure to use for all character/game dialogues
    /// </summary>
    public struct GameDialogue {
        public bool QuestDialogue { private set; get; }
        public int ChapterNumber { private set; get; }
        public string Dialogue { set; get; }

        /// <summary>
        /// The game dialogue object
        /// </summary>
        /// <param name="chapterNumber">The chapter the dialogue should show up on</param>
        /// <param name="questDialogue">The quest number from the given chapter to show up on</param>
        /// <param name="dialogue">The dialogue text</param>
        public GameDialogue(int chapterNumber, bool questDialogue, string dialogue)
            : this() {
            this.QuestDialogue = questDialogue;
            this.ChapterNumber = chapterNumber;
            this.Dialogue = dialogue;
        }
    }

    /// <summary>
    /// Details about the player object
    /// </summary>
    public struct PlayerDetailsStruct {
        public string PlayerName { set; get; }
        public string[] Avatar { set; get; }
        public Color PlayerColor { set; get; }
    }

    /// <summary>
    /// A set direction on the console window
    /// </summary>
    public struct Direction {
        public const byte NumberOfDirections = 8;
        public static Point North = new Point(0, -1);
        public static Point NorthEast = new Point(1, -1);
        public static Point NorthWest = new Point(-1, -1);
        public static Point South = new Point(0, 1);
        public static Point SouthEast = new Point(1, 1);
        public static Point SouthWest = new Point(-1, 1);
        public static Point East = new Point(1, 0);
        public static Point West = new Point(-1, 0);
    }

    /// <summary>
    /// As it sounds, the game state
    /// </summary>
    public enum GameState {
        Gameplay,
        GameplayMenu,
        MainMenu,
    }

    /// <summary>
    /// Public variables that should be globally accessible
    /// </summary>
    public static class GameVariables {
        public static Stack<string> DungeonStack;
        public static List<GameObject> GameObjectsList;

        public static bool ColorsEnabled { set; get; }
        public static bool GameOver { set; get; }
        public static bool GamePaused { set; get; }
        public static bool MusicEnabled { set; get; }
        public static bool RefreshScreen { set; get; }
        public static bool SoundEnabled { set; get; }
        public static int CurrentChapter { set; get; }
        public static string NextLevel { set; get; }
        public static ConsoleFont CurrentFont = ConsoleHelper.ConsoleFonts[8];

        public static Box LevelBorder;
        public static Level CurrentLevel;
        public static PlayerDetailsStruct CurrentMenuPlayerDetails;
        public static Point HudPosition = Point.Zero;
        public static Random Randomizer = new Random(); //TODO: NOT THREAD SAFE

        public static ConsoleKey CurrentKeyPressed { set; get; }
        public static GameState CurrentGameState { set; get; }

        public static readonly float FrameRate = 60;

        public static readonly Color DialogueBorderColor = ItemColors.DefaultConsoleColor;
        public static readonly Color DialogueTextColor = ItemColors.PlainUI;
        public static readonly Color DialogueTitleColor = ItemColors.GoodMessage;
        public static readonly int DialogueWindowHeight = 5; //Enough for 3 lines of text
        public static readonly int DialogueWindowWidth = 40; //66% of the minimum hud width

        public static readonly Color HudColor = ItemColors.PlainUI;
        public static readonly int HudBufferSpace = 3; //For rows/column octothorpes for positioning the HUD and screen
        public static readonly int MinimumHudWidth = 70;
        public static readonly int PlayerAvatarHeightRequirement = 4;
        public static readonly int PlayerAvatarWidthRequirement = 6;

        public static readonly string GameMusicFile = "GameMusic.wav";
        public static readonly string MenuMusicFile = "MenuMusic.wav";
        public static readonly string GameInfoFile = string.Format("{0}\\GameTextFiles\\gameinfo.txt", Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName);
        public static readonly string AvatarsDirectory = string.Format("{0}\\GameTextFiles\\Menus\\", Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName);
        public static readonly string LogoFile = string.Format("{0}\\GameTextFiles\\Menus\\logo.txt", Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName);
        public static readonly string LevelDirectory = string.Format("{0}\\GameTextFiles\\Levels\\", Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName);
        public static readonly string NpcDialogueDirectory = string.Format("{0}\\GameTextFiles\\Dialogues\\", Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName);
        public static readonly string SoundsDirectory = string.Format("{0}\\Sounds\\", Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName);
        public static readonly string SaveConfigurationLocation = string.Format("{0}\\Pork\\", System.Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData));        
        public static readonly string ProgramName = GlobalMethods.GetConfigurationSetting(File.ReadAllLines(GameVariables.GameInfoFile), "GameName");
        public static string SaveGameLocation = string.Format("{0}\\Saved Games\\{1}\\", System.Environment.GetFolderPath(Environment.SpecialFolder.UserProfile), ProgramName);

        public static Action IncrementCurrentChapter;
        public static Action InitializeGameSettings;
        public static Action InitializeTutorial;
    }

    /// <summary>
    /// The controls to play the game with
    /// </summary>
    public static class Controls {
        public const ConsoleKey ActionKey = ConsoleKey.E;
        public const ConsoleKey BackKey = ConsoleKey.B;
        public const ConsoleKey GameplayMenuKey = ConsoleKey.Escape;
        public const ConsoleKey JournalKey = ConsoleKey.J;
        public const ConsoleKey PauseGameKey = ConsoleKey.P;
        public const ConsoleKey ShootKey = ConsoleKey.Spacebar;

        public const ConsoleKey MoveLeft = ConsoleKey.LeftArrow;
        public const ConsoleKey MoveRight = ConsoleKey.RightArrow;
        public const ConsoleKey MoveUp = ConsoleKey.UpArrow;
        public const ConsoleKey MoveDown = ConsoleKey.DownArrow;
        public const ConsoleKey MoveLeftAlternative = ConsoleKey.A;
        public const ConsoleKey MoveRightAlternative = ConsoleKey.D;
        public const ConsoleKey MoveUpAlternative = ConsoleKey.W;
        public const ConsoleKey MoveDownAlternative = ConsoleKey.S;

        public const ConsoleKey InventoryKey = ConsoleKey.I;
        public const ConsoleKey InventoryExitKey = ConsoleKey.Escape;
        public const ConsoleKey InventorySpecificationsKey = ConsoleKey.Spacebar;
        public const ConsoleKey InventoryEquipUseKey = ConsoleKey.E;
    }

    /// <summary>
    /// Sorted by color names since the console window is limited to 16 colors per session (Windows problems!). This means a maximum of 14 rows (below)
    /// </summary>
    public static class ItemColors {
        //1. Blue
        public static readonly Color Water = Color.Blue;
        public static readonly Color PlayerBlue = Color.Blue;

        //2. Cyan
        public static readonly Color NpcRegular = Color.Cyan;

        //3. Dark Cyan
        public static readonly Color Wall = Color.DarkCyan;

        //4. Dark Red
        public static readonly Color TreeStump = Color.DarkRed;

        //5. Green
        public static readonly Color ExitArea = Color.Green;
        public static readonly Color NpcMerchant = Color.Green;

        //6. Red
        public static readonly Color BadMessage = Color.Red;
        public static readonly Color Enemy = Color.Red;
        public static readonly Color Fire1 = Color.Red;

        //7. Light Goldenrod Yellow
        public static readonly Color Hay = Color.LightGoldenrodYellow;

        //8. Lime
        public static readonly Color Equipment = Color.Lime;
        public static readonly Color GoodMessage = Color.Lime;
        public static readonly Color Grass = Color.Lime;
        public static readonly Color PlayerLime = Color.Lime;
        public static readonly Color Tree = Color.Lime;

        //9. Light Gray
        public static readonly Color DefaultConsoleColor = Color.LightGray;
        public static readonly Color Road = Color.LightGray;

        //10. Magenta
        public static readonly Color Potion = Color.Magenta;
        public static readonly Color SelectedItem = Color.Magenta;

        //11. Pink
        public static readonly Color PlayerPink = Color.Pink;

        //12. Saddle Brown
        public static readonly Color Dirt = Color.SaddleBrown;
        public static readonly Color Sign = Color.SaddleBrown;

        //13. White
        public static readonly Color Bullet = Color.White;
        public static readonly Color PlainUI = Color.White;

        //14. Yellow
        public static readonly Color Currency = Color.Yellow;
        public static readonly Color Fire2 = Color.Yellow;
        public static readonly Color PlayerYellow = Color.Yellow;
        public static readonly Color RoadStripe = Color.Yellow;
        public static readonly Color WarningMessage = Color.Yellow;
    }
}
