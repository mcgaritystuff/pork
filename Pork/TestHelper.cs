﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using Console = Colorful.Console;

namespace Pork {
    /// <summary>
    /// This is a public static class to run Test Driven Development.
    /// Note that all methods that we want to test in this class must be private static, return a bool, and start with the word "Test".
    /// </summary>
    public static class TestHelper {
        #region console buffer
        private struct ConsoleSet {
            public string ConsoleSetString { private set; get; }
            public Color ConsoleSetColor { private set; get; }

            public ConsoleSet(string consoleStr, Color consoleColor) : this() {
                ConsoleSetString = consoleStr;
                ConsoleSetColor = consoleColor;
            }
        }

        //In case a test does Console.Clear(), we'll have the buffer saved.
        private static List<ConsoleSet> _consoleBuffer = new List<ConsoleSet>();
        #endregion

        /// <summary>
        /// Simply runs all the test methods given in TestHelper
        /// </summary>
        public static void RunTests() {
            Console.SetWindowSize(90, 30);
            Console.Title = "Pork - Running Tests...";
            List<string> failedMethods = new List<string>();
            bool somethingFailed = false;

            _consoleBuffer.Add(new ConsoleSet("\n", ItemColors.DefaultConsoleColor));
            Console.WriteLine();

            foreach (var item in typeof(TestHelper).GetMethods(BindingFlags.Static | BindingFlags.NonPublic).Where(method => method.Name.ToLower().StartsWith("test"))) {
                GameVariables.GameObjectsList = new List<GameObject>();
                HelperHelperTestWriteLine(String.Format("Running {0}... ", item.Name), ItemColors.PlainUI);
                bool testPassed = (bool)item.Invoke(item, null);

                if (testPassed) {
                    HelperHelperTestWriteLine(String.Format("{0} Passed!\n", item.Name), ItemColors.GoodMessage);
                } else {
                    HelperHelperTestWriteLine(String.Format("{0} Failed!\n", item.Name), ItemColors.BadMessage);
                    somethingFailed = true;
                    failedMethods.Add(item.Name);
                }
            }

            if (somethingFailed) {
                HelperHelperTestWriteLine("\nThe following methods failed:", ItemColors.WarningMessage);
                foreach (var item in failedMethods) {
                    HelperHelperTestWriteLine(String.Format(" * {0}", item), ItemColors.BadMessage);
                }
            } else {
                HelperHelperTestWriteLine(
                    String.Format("\n{0}\n{1}\n{0}", "------------------",
                                                     "All tests passed!!"), ItemColors.GoodMessage);
            }

            Console.ReadLine();

            _consoleBuffer.Clear();
        }

        #region Test Methods
        private static bool TestEnemy() {
            try {
                Point oldPosition;

                PrintTest("Creating NormalEnemy");
                NormalEnemy enemy = new NormalEnemy(Point.Zero, 'E', Color.Black, 1, 5);
                if (enemy.ObjectColor != Color.Black || enemy.MoveSpeed != 1 ||
                    enemy.Icon != 'E' || !enemy.Position.Equals(Point.Zero) ||
                    !GameVariables.GameObjectsList.Contains(enemy)) {
                    throw new Exception("Enemy creation unsuccessful");
                }

                PrintSuccess();

                PrintTest("Running one enemy Update()");
                oldPosition = new Point(Console.CursorLeft, Console.CursorTop);
                enemy.Update();
                Console.SetCursorPosition(oldPosition.X, oldPosition.Y);
                PrintSuccess();

                PrintTest("Calling enemy.Draw()");
                oldPosition = new Point(Console.CursorLeft, Console.CursorTop);
                enemy.Draw();
                Console.SetCursorPosition(oldPosition.X, oldPosition.Y);
                PrintSuccess();

                //Go through "one frame" (but not one second)
                for (int i = 0; i < GameVariables.FrameRate + 1; i++) ;

                PrintTest("Calling enemy.TakeDamage(1)");
                enemy.TakeDamage(1);
                PrintSuccess();

                PrintTest("Calling enemy.Destroy()");
                oldPosition = new Point(Console.CursorLeft, Console.CursorTop);
                enemy.Destroy();
                if (GameVariables.GameObjectsList.Contains(enemy)) {
                    throw new Exception("Enemy wasn't removed from GameObjectsList");
                }
                Console.SetCursorPosition(oldPosition.X, oldPosition.Y);
                PrintSuccess();
            } catch (Exception error) {
                PrintFailure(error.Message);
                return false;
            }

            return true;
        }

        private static bool TestPlayer() {
            try {
                Point oldPosition;

                PrintTest("Creating Player");
                Player player = new Player(Point.Zero, 'P', Color.Yellow);
                player.AvatarImage = new string[] { "AAAAAA", "AAAAAA", "AAAAAA", "AAAAAA", };
                GameVariables.CurrentLevel = new Level("TEST LEVEL", new string[] { "###" });
                if (!player.Position.Equals(Point.Zero) || player.Icon != 'P' ||
                    player.ObjectColor != Color.Yellow ||
                    !GameVariables.GameObjectsList.Contains(player)) {
                    throw new Exception("Player creation unsuccessful");
                }
                PrintSuccess();

                PrintTest("Running one player Update()");
                oldPosition = new Point(Console.CursorLeft, Console.CursorTop);
                player.Update();
                Console.SetCursorPosition(oldPosition.X, oldPosition.Y);
                PrintSuccess();

                PrintTest("Calling player.Draw()");
                oldPosition = new Point(Console.CursorLeft, Console.CursorTop);
                player.Draw();
                Console.SetCursorPosition(oldPosition.X, oldPosition.Y);
                PrintSuccess();

                //Go through "one frame" (but not one second)
                for (int i = 0; i < GameVariables.FrameRate + 1; i++) ;

                PrintTest("Calling player.TakeDamage(1)");
                player.TakeDamage(1);
                PrintSuccess();
                Console.Clear();
                PrintConsoleBuffer();

                PrintTest("Calling player.SwapWeapon");
                Shotgun gun = new Shotgun(3, 3, 3, null, Point.Zero, ' ');
                player.SwapWeapon(gun);
                if (player.CurrentWeapon != gun) {
                    throw new Exception("SwapWeapon unsuccessful");
                }
                PrintSuccess();
                Console.Clear();
                PrintConsoleBuffer();

                PrintTest("Calling player.Destroy()");
                oldPosition = new Point(Console.CursorLeft, Console.CursorTop);
                player.Destroy();
                if (GameVariables.GameObjectsList.Contains(player)) {
                    throw new Exception("Player wasn't removed from GameObjectsList");
                }
                Console.SetCursorPosition(oldPosition.X, oldPosition.Y);
                PrintSuccess();
            } catch (Exception error) {
                PrintFailure(error.Message);
                return false;
            }

            return true;
        }

        private static bool TestLevelCreation() {
            try {
                string[] lvlArr = new string[1] { "WEP#asdf" };

                PrintTest("Creating Level Object with string[1] {{ {0} }}", lvlArr);
                Level lvl = new Level("testlvl.txt", lvlArr);
                lvl.CreateLevel(null);
                //enemy, player, wall, and player's pistol
                if (GameVariables.GameObjectsList.Count != 4) {
                    throw new Exception("Not all objects in level created");
                }
                PrintSuccess();

                PrintTest("Finishing level");
                lvl.FinishLevel();
                if (Console.CursorTop != 0 || Console.CursorLeft != 0) {
                    throw new Exception("Console wasn't cleared in level");
                }
                PrintConsoleBuffer(); //FinishLevel() does Console.Clear
                if (GameVariables.GameObjectsList.Count > 0) {
                    throw new Exception("Level not finished. There are still GameObjects");
                }
                PrintSuccess();
            } catch (Exception error) {
                PrintFailure(error.Message);
                return false;
            }

            return true;
        }

        private static bool TestWeapons() {
            try {
                PrintTest("Creating Pistol(1,1,1)");
                Pistol pistol = new Pistol(1, 1, 1, null, Point.Zero, ' ');
                if (pistol.MaxShootTimer != 1) {
                    throw new Exception("Parameter MaxShootTimer did not set to 1");
                } else if (pistol.BulletSpeed != 1) {
                    throw new Exception("Parameter BulletSpeed did not set to 1");
                } else if (pistol.WeaponDamage != 1) {
                    throw new Exception("Parameter WeaponDamage did not set to 1");
                }
                PrintSuccess();

                PrintTest("Running update on pistol to go through one frame");
                for (int i = 0; i < pistol.MaxShootTimer; i++)
                    pistol.Update();
                if (pistol.CurrentShootTimer == 0)
                    throw new Exception("Pistol did not update CurrentShootTimer");
                PrintSuccess();

                PrintTest("Creating Shotgun(1,1,1)");
                Shotgun shotgun = new Shotgun(1, 1, 1, null, Point.Zero, ' ');
                if (shotgun.MaxShootTimer != 1) {
                    throw new Exception("Parameter MaxShootTimer did not set to 1");
                } else if (shotgun.BulletSpeed != 1) {
                    throw new Exception("Parameter BulletSpeed did not set to 1");
                } else if (shotgun.WeaponDamage != 1) {
                    throw new Exception("Parameter WeaponDamage did not set to 1");
                }
                PrintSuccess();

                PrintTest("Running update on shotgun to go through one frame");
                for (int i = 0; i < shotgun.MaxShootTimer; i++) {
                    shotgun.Update();
                }
                if (shotgun.CurrentShootTimer == 0) {
                    throw new Exception("Shotgun did not update CurrentShootTimer");
                }
                PrintSuccess();

                PrintTest("Calling pistol.Shoot with direction East at 0,0 and as an enemy bullet");
                pistol.Shoot(Direction.East, Point.Zero, true);
                if (GameVariables.GameObjectsList.Count == 0) {
                    throw new Exception("Bullet wasn't added to GameObjectsList");
                }
                PrintSuccess();

                GameVariables.GameObjectsList.Clear(); //Clear bullets
            } catch (Exception error) {
                PrintFailure(error.Message);
                return false;
            }

            return true;
        }

        private static bool TestBullets() {
            try {
                Point oldPosition;

                PrintTest("Creating a bullet going East, 1 BulletSpeed & Damage, starting at (0,0), and true to being an enemy bullet.");
                var bullet = new Bullet(Direction.East, 1, Point.Zero, true, 1);
                if (!bullet.Position.Equals(Point.Zero)) {
                    throw new Exception("Bullet wasn't placed where expected.");
                }
                if (!GameVariables.GameObjectsList.Contains(bullet)) {
                    throw new Exception("Bullet didn't get added to GameObjectsList");
                }
                PrintSuccess();

                PrintTest("Updating bullet for 1 frame");
                for (int i = 0; i < GameVariables.FrameRate; i++) {
                    bullet.Update();
                }
                if (bullet.Position.Equals(Point.Zero)) {
                    throw new Exception("Bullet didn't move as expected.");
                }
                PrintSuccess();

                PrintTest("Drawing Bullet");
                oldPosition = new Point(Console.CursorLeft, Console.CursorTop);
                bullet.Draw();
                Console.SetCursorPosition(oldPosition.X, oldPosition.Y);
                PrintSuccess();

                PrintTest("Destroying Bullet");
                oldPosition = new Point(Console.CursorLeft, Console.CursorTop);
                bullet.Destroy();
                Console.SetCursorPosition(oldPosition.X, oldPosition.Y);
                if (GameVariables.GameObjectsList.Contains(bullet)) {
                    throw new Exception("Bullet is still in the GameObjectsList!");
                }
                PrintSuccess();
            } catch (Exception error) {
                PrintFailure(error.Message);
                return false;
            }

            return true;
        }

        #endregion

        #region print methods
        private static void PrintTest(string testString) {
            HelperTestWrite(String.Format(" * Testing {0}... ", testString), ItemColors.PlainUI);
        }

        private static void PrintTest(string formatter, params string[] strs) {
            HelperTestWrite(String.Format(" * Testing {0}... ", String.Format(formatter, strs)), ItemColors.PlainUI);
        }

        private static void PrintSuccess() {
            HelperHelperTestWriteLine("Success!", ItemColors.GoodMessage);
        }

        private static void PrintFailure(string failMessage) {
            HelperHelperTestWriteLine(String.Format("Fail! Error: {0}", failMessage), ItemColors.BadMessage);
        }

        private static void HelperTestWrite(string printString, Color color) {
            Console.Write(printString.WordWrap(false), color);
            _consoleBuffer.Add(new ConsoleSet(printString.WordWrap(false), color));
        }

        private static void HelperHelperTestWriteLine(string printString, Color color) {
            Console.WriteLine(printString.WordWrap(false), color);
            _consoleBuffer.Add(new ConsoleSet(printString.WordWrap(false) + "\n", color));
        }

        private static void PrintConsoleBuffer() {
            foreach (var item in _consoleBuffer) {
                Console.Write(item.ConsoleSetString, item.ConsoleSetColor);
            }
        }

        #endregion
    }
}
