﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CharShooter
{
    class BasicLevel
    {
        public int Height { private set; get; }
        public int Width { private set; get; }
        public int NumberOfEnemies { private set; get; }
        public int NumberOfRandomWalls { private set; get; }

        //To keep solids a certain distance from the edge of the level
        private static readonly int LevelSolidEdgeGap = 5;

        public BasicLevel(int width, int height, int numOfEnemies, int numOfRandWalls)
        {
            Width = width;
            Height = height;
            NumberOfEnemies = numOfEnemies;
            NumberOfRandomWalls = numOfRandWalls;
        }

        public void CreateLevel()
        {
            for (int i = 0; i < Width; i++)
            {
                new Wall(new Point(i, 0));
                new Wall(new Point(i, Height - 1));
            }
            for (int i = 1; i < Height - 1; i++)
            {
                new Wall(new Point(0, i));
                new Wall(new Point(Width - 1, i));
            }
            CreateRandomEnemies();
            CreateRandomWalls();
            new Player(new Point(1, 1), '0', ConsoleColor.Green);
        }

        public void FinishLevel()
        {
            GlobalMethods.ClearAllLists();
        }

        private void CreateRandomEnemies()
        {
            for (int i = 0; i < NumberOfEnemies; i++)
            {
                Point position = new Point(GameVariables.randomizer.Next(LevelSolidEdgeGap, Width - LevelSolidEdgeGap),
                                           GameVariables.randomizer.Next(LevelSolidEdgeGap, Height - LevelSolidEdgeGap));
                new NormalEnemy(position, 'X', ConsoleColor.Red, 1);
            }
        }

        private void CreateRandomWalls()
        {
            for (int i = 0; i < NumberOfRandomWalls; i++)
            {
                Point position = new Point(GameVariables.randomizer.Next(LevelSolidEdgeGap, Width - LevelSolidEdgeGap),
                                           GameVariables.randomizer.Next(LevelSolidEdgeGap, Height - LevelSolidEdgeGap));
                new Wall(position);
            }
        }
    }
}
