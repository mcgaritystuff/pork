﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace Pork {
    public class Level {
        private string[] _mapLines;
        private Level _previousLevel;

        public bool HasPlayerStartPosition { private set; get; }
        public int LevelHeight { private set; get; }
        public int LevelWidth { private set; get; }
        public string LevelName { private set; get; }

        /// <summary>
        /// Setup a new default level object (must call CreateLevel to actually... well, create it)
        /// </summary>
        /// <param name="levelName">The name of the level</param>
        /// <param name="mapLines">A string array to draw in the console for the level</param>
        public Level(string levelName, string[] mapLines = null) {
            _mapLines = mapLines;
            HasPlayerStartPosition = false;
            LevelName = levelName;
        }

        /// <summary>
        /// Create the level!
        /// </summary>
        /// <param name="previousLevel">Pass in the previous level to save player positioning and to determine dungeon/map calculations</param>
        /// <param name="listOfObjects">If this is loading from a previous state (i.e. cached somewhere), pass in the entire cached game objects list to restore them</param>
        public void CreateLevel(Level previousLevel, List<GameObject> listOfObjects = null) {
            _previousLevel = previousLevel;

            ExitArea hittingExitArea = null;
            List<GameObject> players = SavePlayerPositions(ref hittingExitArea);

            try {
                // If this is the first time this level has been stepped into, draw it from scratch. Otherwise
                // load it from the cached list provided
                if (listOfObjects == null) {
                    DrawLevel();
                } else {
                    GameVariables.GameObjectsList = listOfObjects;
                    GameVariables.GameObjectsList.RemoveAll(obj => obj is IPlayer);

                    LevelWidth = GameVariables.GameObjectsList.Max(obj => obj.Position.X) + 1;
                    LevelHeight = GameVariables.GameObjectsList.Max(obj => obj.Position.Y) + 1;
                }
            } catch (Exception e) {
                throw new Exception("Exception: " + e.Message);
            }

            // Set the player where they walked into the level at
            SetPlayerPositioning(hittingExitArea, players, (listOfObjects == null ? false : true));
        }

        /// <summary>
        /// Clears the game buffer
        /// </summary>
        public void FinishLevel() {
            GlobalMethods.ClearGameBuffer();
        }

        /// <summary>
        /// Place all fresh objects in their places on the level.
        /// </summary>
        private void DrawLevel() {
            Point currMapPos = Point.Zero;
            bool comment = false;
            List<Point> _exitPoints = new List<Point>();
            Dictionary<int, Point> _npcPoints = new Dictionary<int, Point>();

            foreach (var item in _mapLines) {
                // Reached the end of the textfile *drawing* part
                if (item.ToUpperInvariant().StartsWith("ENDLEVEL")) {
                    break;
                }

                #region quest-related vars
                bool showAtChapter = false;
                int chapter = 0;
                #endregion

                foreach (var c in item) {
                    int result = 0;
                    // NPCs are labeled 1-9
                    if (Int32.TryParse(c.ToString(), out result)) {
                        _npcPoints.Add(result, currMapPos);
                    }

                    switch (char.ToUpperInvariant(c)) {
                        case '#':
                            comment = true;
                            break;
                        case '~': //Water
                            new Water(currMapPos, '≈', ItemColors.Water);
                            break;
                        case 'B': //tree stump and bridge wall
                            new Wall(currMapPos, '║', ItemColors.TreeStump);
                            break;
                        case 'D'://dirt
                            new Ground(currMapPos, '░', ItemColors.Dirt);
                            break;
                        case 'E': //normal enemy
                            new NormalEnemy(currMapPos, '☻', ItemColors.Enemy, 1, 5);
                            break;
                        case 'F'://fire
                            new Fire(currMapPos, '░', ItemColors.Fire1, ItemColors.Fire2);
                            break;
                        case 'G': //Grass
                            new Ground(currMapPos, '▒', ItemColors.Grass);
                            break;
                        case 'H': //Hay
                            new Ground(currMapPos, '▒', ItemColors.Hay);
                            break;
                        case 'P': //player
                            var p = new Player(currMapPos, '☺', GameVariables.CurrentMenuPlayerDetails.PlayerColor);
                            p.AvatarImage = GameVariables.CurrentMenuPlayerDetails.Avatar;
                            p.PlayerName = GameVariables.CurrentMenuPlayerDetails.PlayerName;
                            HasPlayerStartPosition = true;
                            break;
                        case 'Q'://roadstripe
                            new Ground(currMapPos, '■', ItemColors.RoadStripe);
                            break;
                        case 'R'://road/stairs
                            new Ground(currMapPos, '▒', ItemColors.Road);
                            break;
                        case 'S': //Sign
                            new Sign(currMapPos, '╤', ItemColors.Sign, GetSignText());
                            break;
                        case 'T': //tree
                            new Wall(currMapPos, '#', ItemColors.Tree);
                            break;
                        case 'W': //wall
                            new Wall(currMapPos, '#', ItemColors.Wall);
                            break;
                        case 'X': //Exit to a different level
                            _exitPoints.Add(currMapPos); //add to a list for now. Just until we have map width/height
                            break;
                        case 'Y': //Quest wall
                            SetQuestInfo(ref showAtChapter, ref chapter, "QUEST_WALL");
                            new QuestWall(currMapPos, '#', ItemColors.Wall, chapter, showAtChapter);
                            break;
                        case 'Z': //Quest Item
                            SetQuestInfo(ref showAtChapter, ref chapter, "QUEST_ITEM");
                            CreateQuestItem(currMapPos, showAtChapter, chapter);
                            break;
                        case '&': //Quest Enemy
                            SetQuestInfo(ref showAtChapter, ref chapter, "QUEST_ENEMY");
                            new QuestEnemy(currMapPos, '☻', ItemColors.Enemy, 1, 10, chapter, showAtChapter);
                            break;
                        default: //unrecognizable character or space
                            break;
                    }

                    //A comment was made, ignore the rest of the line
                    if (comment) {
                        break;
                    }

                    currMapPos = new Point(currMapPos.X + 1, currMapPos.Y);
                }

                //Don't add lines on screen for comments!
                if (!comment) {
                    currMapPos = new Point(0, currMapPos.Y + 1);
                }

                comment = false;
            }

            LevelWidth = GameVariables.GameObjectsList.Max(obj => obj.Position.X) + 1;
            LevelHeight = GameVariables.GameObjectsList.Max(obj => obj.Position.Y) + 1;

            // Now that the map is drawn, define all the exits possible as well as all the npcs
            foreach (var item in _exitPoints) {
                DefineExit(item);
            }
            foreach (var item in _npcPoints) {
                DefineNPC(item.Key, item.Value);
            }
        }

        /// <summary>
        /// Save all the positions that the player or players are in.
        /// </summary>
        /// <param name="hittingExitArea">To reference the exit area that the player collided with</param>
        /// <returns>A list of all the players</returns>
        private List<GameObject> SavePlayerPositions(ref ExitArea hittingExitArea, List<GameObject> gameObjectsList = null) {
            List<GameObject> players = new List<GameObject>();

            //If this is us coming from a previous level and not the main menu, save the exit 
            //area we're colliding with, the players, and finish the level
            if (_previousLevel != null) {
                hittingExitArea = (GameVariables.GameObjectsList.First(exitArea => exitArea is ExitArea && (exitArea as ExitArea).CollidingPlayer != null)) as ExitArea;

                if (gameObjectsList == null) {
                    players = GameVariables.GameObjectsList.Where(obj => obj is IPlayer).ToList();
                } else {
                    players = gameObjectsList.Where(obj => obj is IPlayer).ToList();
                }

                _previousLevel.FinishLevel();
            }

            return players;
        }

        /// <summary>
        /// Place the player or players in the positions necessary. Note: all players will be placed on the same location.
        /// </summary>
        /// <param name="hittingExitArea">The exit area that was hit (determines new positioning for players)</param>
        /// <param name="players">All the players to position</param>
        /// <param name="fromCache">If this was cached, we don't want to create multiple players! Set to true if this for a cached set</param>
        private void SetPlayerPositioning(ExitArea hittingExitArea, List<GameObject> players, bool fromCache) {
            //Continuing, if we're transitioning into a new level, we need to set every player's position
            //to the corresponding position desired
            Point newStartPosition = new Point(-1, -1);

            //If we're moving the player to a position corresponding to the exit area
            if (hittingExitArea != null) {
                // Start the player where they came onto the screen from. Otherwise depend on cache/file
                if (hittingExitArea.ExitMapSide.Equals(Direction.North)) {
                    newStartPosition = new Point(hittingExitArea.Position.X, LevelHeight - 1);
                } else if (hittingExitArea.ExitMapSide.Equals(Direction.South)) {
                    newStartPosition = new Point(hittingExitArea.Position.X, 0);
                } else if (hittingExitArea.ExitMapSide.Equals(Direction.West)) {
                    newStartPosition = new Point(LevelWidth - 1, hittingExitArea.Position.Y);
                } else if (hittingExitArea.ExitMapSide.Equals(Direction.East)) {
                    newStartPosition = new Point(0, hittingExitArea.Position.Y);
                } else {
                    if (fromCache) {
                        newStartPosition = findPlayerMapPositionFromCache();
                    } else {
                        newStartPosition = GameVariables.GameObjectsList.First(obj => obj is IPlayer).Position;
                    }

                    if (hittingExitArea.NameOfExitLevel != "EXIT") {
                        GameVariables.DungeonStack.Push(_previousLevel.LevelName);
                    }
                }

                GameVariables.GameObjectsList.RemoveAll(obj => obj is IPlayer);
            }

            //Set all the current players to the new position
            foreach (IPlayer player in players) {
                //Was removed during previousLevel.FinishLevel();
                GameVariables.GameObjectsList.Add(player as GameObject); 

                //Only move the players if the new map doesn't have a start position defined already
                if (!newStartPosition.Equals(new Point(-1, -1))) {
                    player.ForceNewPosition(newStartPosition);
                }
            }

            GameVariables.NextLevel = null;
        }

        /// <summary>
        /// Checking the current cache, we need to see if we can find the player on the map with the P icon and then return that point
        /// </summary>
        /// <returns>The point given where the P letter is found. Otherwise returns Point(0,0)</returns>
        private Point findPlayerMapPositionFromCache() {
            int x = 0;
            int y = 0;
            string currLine = "";

            for (int i = 0; i < _mapLines.Length; i++) {
                if (_mapLines[i].Contains('P')) {
                    currLine = _mapLines[i];
                    y = i;
                    break;
                }
            }

            for (int i = 0; i < currLine.Length; i++) {
                if (currLine[i] == 'P') {
                    x = i;
                    break;
                }
            }

            return new Point(x, y);
        }

        /// <summary>
        /// Create the exit at the given map position
        /// </summary>
        /// <param name="mapPosition">The point to place the exit</param>
        private void DefineExit(Point mapPosition) {
            string mapLine = "";
            string direction = "";
            Point directionPoint = Point.Zero;

            if (mapPosition.X == 0) { //Go west
                direction = "WEST";
                directionPoint = Direction.West;
            } else if (mapPosition.X == LevelWidth - 1) { //Go east              
                direction = "EAST";
                directionPoint = Direction.East;
            } else if (mapPosition.Y == 0) { //Go North
                direction = "NORTH";
                directionPoint = Direction.North;
            } else if (mapPosition.Y == LevelHeight - 1) { //Go South
                direction = "SOUTH";
                directionPoint = Direction.South;
            } else { //Dungeon 
                direction = "DUNGEON";
            }

            mapLine = _mapLines.FirstOrDefault(line => line.ToUpperInvariant().StartsWith(direction));

            if (mapLine != null) {
                string[] strings = mapLine.Split('=');

                //Name = Filename
                if (strings.Length == 2) {
                    strings[1].Replace(" ", "");

                    if (direction.Equals("DUNGEON") && strings[1].Equals("EXIT")) {
                        new ExitArea(mapPosition, strings[1], directionPoint, false);
                    } else {
                        new ExitArea(mapPosition, strings[1], directionPoint, (direction.Equals("DUNGEON") ? true : false));
                    }
                }
            }
        }

        /// <summary>
        /// Create the NPC at the given map position
        /// </summary>
        /// <param name="NpcNumber">The number represented on the map</param>
        /// <param name="mapPosition">Where to place the NPC</param>
        private void DefineNPC(int NpcNumber, Point mapPosition) {
            string mapLine = "";

            mapLine = _mapLines.FirstOrDefault(line => line.ToUpperInvariant().StartsWith(NpcNumber.ToString()));

            if (mapLine != null) {
                string[] strings = mapLine.Split('=');

                //Number = NPC Type = NPC Name
                if (strings.Length == 3) {
                    for (int i = 0; i < strings.Length; i++) {
                        strings[i].Replace(" ", "");
                    }

                    CoreNPCObject currNpc = null;

                    //NOTE: We *could* do reflection, but because it's so costly and not ideal for this project, it would be better to just do this switch statement.
                    switch (strings[1]) {
                        case "RegularNpcCharacter":
                            RegularNpcCharacter regCharacter = new RegularNpcCharacter('☺', ItemColors.NpcRegular, strings[2].SplitByCapitalLetters(), mapPosition);
                            setCharacterDialogue(strings, regCharacter);
                            currNpc = regCharacter;
                            break;
                        case "MerchantNpcCharacter":
                            MerchantNpcCharacter merchantCharacter = new MerchantNpcCharacter('☺', ItemColors.NpcMerchant, strings[2].SplitByCapitalLetters(), mapPosition);
                            setCharacterDialogue(strings, merchantCharacter);
                            break;
                        default:
                            break;
                    }

                }
            }
        }

        private void setCharacterDialogue(string[] strings, CoreNPCObject npc) {
            string fileName = string.Format("{0}\\{1}.txt", GameVariables.NpcDialogueDirectory, strings[2]);

            //Read the dialogue text file of the character and add it to the new character
            if (File.Exists(fileName)) {
                var textLines = File.ReadAllLines(fileName).Where(str => !str.StartsWith("#")); //Ignore comments

                foreach (var line in textLines) {
                    var currLine = line.Split(new string[] { "=>" }, StringSplitOptions.None);

                    //chapter => quest? => dialgue => quest text
                    if (currLine.Length == 4) {
                        npc.AddDialogue(new GameDialogue(Int32.Parse(currLine[0]),
                                                         Boolean.Parse(currLine[1]), currLine[2]));
                        if (!string.IsNullOrEmpty(currLine[3])) {
                            QuestLog.AddQuest(Int32.Parse(currLine[0]), currLine[3].ReplaceKeywords(), npc.NpcName);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Simply looks for the SIGN tag in the map file and returns the string from it
        /// </summary>
        /// <returns>The text found on the sign, otherwise returns a generic statement</returns>
        private string GetSignText() {
            string mapLine = "";

            mapLine = _mapLines.FirstOrDefault(line => line.ToUpperInvariant().StartsWith("SIGN"));

            if (mapLine != null) {
                string[] strings = mapLine.Split('=');
                if (strings.Length == 2) {
                    return string.Format("\"{0}\"", strings[1]);
                }
            }

            return "There's nothing posted on the sign.";
        }

        /// <summary>
        /// Get the quest information for the level and assign it to showAtChapter and chapter
        /// </summary>
        /// <param name="showAtChapter">Reference to set the boolean to from the level file</param>
        /// <param name="chapter">Reference to set the number to from the level file</param>
        /// <param name="questType">The type of quest that should be searched for in the document</param>
        private void SetQuestInfo(ref bool showAtChapter, ref int chapter, string questType) {
            string mapLine = "";

            mapLine = _mapLines.FirstOrDefault(line => line.ToUpperInvariant().StartsWith(questType));

            if (mapLine != null) {
                string[] strings = mapLine.Split('=');
                if (strings.Length == 3) {
                    if (!Boolean.TryParse(strings[1], out showAtChapter)) {
                        throw new Exception("Parsing quest wall information on level was invalid. Check hide/show string");
                    }
                    if (!Int32.TryParse(strings[2], out chapter)) {
                        throw new Exception("Parsing quest wall information on level was invalid. Check chapter string");
                    }
                }
            }
        }

        /// <summary>
        /// Create a new quest-related item
        /// </summary>
        /// <param name="mapPos">The place to create it</param>
        /// <param name="showAtChapter">Whether to show it on the chapter given or remove it</param>
        /// <param name="chapter">The chapter to show it or delete it</param>
        private void CreateQuestItem(Point mapPos, bool showAtChapter, int chapter) {
            string mapLine = "";

            mapLine = _mapLines.FirstOrDefault(line => line.ToUpperInvariant().StartsWith("YDETAILS"));

            if (mapLine != null) {
                string[] strings = mapLine.Split('=');
                if (strings.Length >= 3) {
                    GameObject item = null;

                    if (strings[1] == "WEAPON") {
                        switch (strings[2]) {
                            case "PistolNormal":
                                item = GameWeapons.PistolNormal(mapPos);
                                break;
                            case "ShotgunNormal":
                                item = GameWeapons.ShotgunNormal(mapPos);
                                break;
                            case "ShotgunStrong":
                                item = GameWeapons.ShotgunStrong(mapPos);
                                break;
                        }
                    } else if (strings[1] == "ARMOR") {
                        switch (strings[2]) {
                            case "CowHideNormal":
                                item = GameArmors.CowHideNormal(mapPos);
                                break;
                            case "CowHideHealthy":
                                item = GameArmors.CowHideHealthy(strings.Length >= 4 ? Int32.Parse(strings[3]) : 5, mapPos);
                                break;
                            case "CowHideGodly":
                                item = GameArmors.CowHideGodly(strings.Length >= 4 ? Int32.Parse(strings[3]) : 15, strings.Length >= 5 ? Int32.Parse(strings[4]) : 5, mapPos);
                                break;
                        }
                    } else if (strings[1] == "ITEM") {
                        switch (strings[2]) {
                            case "SmallHealPotion":
                                item = GameItems.SmallHealPotion(mapPos);
                                break;
                            case "MediumHealPotion":
                                item = GameItems.MediumHealPotion(mapPos);
                                break;
                            case "LargeHealPotion":
                                item = GameItems.LargeHealPotion(mapPos);
                                break;
                            case "ExtraLargeHealPotion":
                                item = GameItems.ExtraLargeHealPotion(mapPos);
                                break;
                        }
                    }

                    if (item != null) {
                        GameVariables.IncrementCurrentChapter += (() => item.Enabled = GlobalMethods.ContinueUpdateOrDraw(item, showAtChapter, chapter));
                        item.Enabled = GlobalMethods.ContinueUpdateOrDraw(item, showAtChapter, chapter);
                    }
                }
            }
        }
    }
}
