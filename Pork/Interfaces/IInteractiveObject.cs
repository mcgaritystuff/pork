﻿namespace Pork {
    interface IInteractiveObject {
        void Interact(IPlayer player);
    }
}
