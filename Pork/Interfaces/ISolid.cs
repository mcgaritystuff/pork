﻿using System;
namespace Pork {
    public interface ISolid {
        bool SolidActivated { get; }
    }
}
