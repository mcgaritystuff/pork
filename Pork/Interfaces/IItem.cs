﻿namespace Pork {
    public interface IItem : IDestroyable {
        string ItemDescription { get; }
        string ItemName { get; }

        void Hide();
        void UseItem();
    }
}
