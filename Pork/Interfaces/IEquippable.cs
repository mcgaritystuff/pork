﻿using System.Collections.Generic;

namespace Pork {
    public interface IEquippable {
        List<IMagicAttribute> MagicAttributes { get; }
        bool IsEquipped { get; }
        string EquipmentDescription { get; }
        string EquipmentName { get; }

        void EquipItem(IPlayer owner);
        void Hide();
        void UnequipItem(IPlayer owner);
    }
}
