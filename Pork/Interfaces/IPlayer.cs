﻿namespace Pork {
    public interface IPlayer : ICreature {
        bool JustSpawned { get; }
        uint Currency { get; }
        string PlayerName { get; }
        string[] AvatarImage { get; set; }
        Inventory PlayerInventory { get; }

        void ForceNewPosition(Point newPosition);
        void GainCurrency(uint value);
        bool SpendCurrency(uint value);
        void SwapWeapon(IWeapon newWeapon);
    }
}
