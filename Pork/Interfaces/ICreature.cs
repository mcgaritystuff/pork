﻿namespace Pork {
    public interface ICreature {
        int Health { get; }
        int MaxHealth { set; get; }
        IWeapon CurrentWeapon { get; }
        Point CreatureDirection { get; }

        void Heal(int health);
        void TakeDamage(int damage);
    }
}
