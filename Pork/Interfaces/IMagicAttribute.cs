﻿using System;

namespace Pork {
    public interface IMagicAttribute {
        Action ActivateAttribute { get; }
        Action DeactivateAttribute { get; }
        bool Passive { get; }
        string Description { get; }
        string MagicNameExtension { get; }

        void SetPlayerOwner(IPlayer owner);
    }
}
