﻿namespace Pork {
    public interface IEnemy : ICreature {
        int Damage { get; }
        uint MoveSpeed { get; }
    }
}
