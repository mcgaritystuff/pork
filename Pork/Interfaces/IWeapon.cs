﻿namespace Pork {
    public interface IWeapon : IEquippable {
        uint CurrentShootTimer { get; }
        uint MaxShootTimer { get; }
        int BulletSpeed { set; get; }
        int WeaponDamage { set; get; }

        void Update();
        void Shoot(Point direction, Point position, bool enemyBullet);
    }
}
