﻿namespace Pork {
    public interface IMenu {
        string MenuTitle { get; }

        void DisplayMenu();
        IMenu SelectMenu(int choiceSelected);
    }
}
