﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CharShooter
{
    public interface ILevel
    {
        int Width { get; }
        int Height { get; }
        int NumberOfEnemies { get; }
        int NumberOfRandomWalls { get; }

        void CreateLevel();
        void FinishLevel();
    }
}
