﻿namespace Pork {
    public interface IMenuItem {
        void ActivateItem();
        void DrawMenuItem();
    }
}
