﻿namespace Pork {
    public interface IDestroyable {
        bool WaitingRemoval { get; }

        void Destroy();
    }
}
