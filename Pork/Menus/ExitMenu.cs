﻿namespace Pork {
    class ExitMenu : IMenu {
        public string MenuTitle { private set; get; }

        /// <summary>
        /// Creates an exit menu object
        /// </summary>
        /// <param name="menuTitle">The title of the menu</param>
        public ExitMenu(string menuTitle) {
            MenuTitle = menuTitle;
        }

        /// <summary>
        /// Exit the game (kills the entire game)
        /// </summary>
        public void DisplayMenu() {
            System.Environment.Exit(0);
        }

        /// <summary>
        /// Does nothing
        /// </summary>
        /// <param name="menuSelected">N/A</param>
        /// <returns>Always returns null</returns>
        public IMenu SelectMenu(int menuSelected) {
            return null;
        }
    }
}
