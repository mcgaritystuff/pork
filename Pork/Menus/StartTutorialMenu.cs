﻿using System;

namespace Pork {
    class StartTutorialMenu : IMenu {
        public string MenuTitle { private set; get; }

        /// <summary>
        /// Create the tutorial start menu
        /// </summary>
        /// <param name="menuTitle">The title of the menu</param>
        public StartTutorialMenu(string menuTitle) {
            MenuTitle = menuTitle;
        }

        /// <summary>
        /// Clear the console and go to GameState.GamePlay. Initializes tutorial
        /// </summary>
        public void DisplayMenu() {
            Console.Clear();
            GameVariables.CurrentGameState = GameState.Gameplay;
            GameVariables.InitializeTutorial();
        }

        /// <summary>
        /// Does nothing
        /// </summary>
        /// <param name="menuSelected">N/A</param>
        /// <returns>Always returns null</returns>
        public IMenu SelectMenu(int menuSelected) {
            return null;
        }
    }
}
