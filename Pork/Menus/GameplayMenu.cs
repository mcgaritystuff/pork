﻿using System;
using System.Collections.Generic;

namespace Pork {
    class GameplayMenu : IMenu {
        public string MenuTitle { private set; get; }

        private List<IMenu> _menuList;

        /// <summary>
        /// Create the gameplay menu
        /// </summary>
        public GameplayMenu() {
            MenuTitle = "Main Menu";
            _menuList = new List<IMenu>() {
                new ContinueGameMenu("Continue Game"),
                new RegularMenu("Save Game (Does not work)", new List<IMenuItem>() { new SaveGameMenuItem(0), new SaveGameMenuItem(1), new SaveGameMenuItem(2) }, new List<IMenu>()),
                new RegularMenu("Options", new List<IMenuItem>() { new ToggleSoundsMenuItem(), new ToggleMusicMenuItem(),
                                                                   new ToggleColorsMenuItem(), new ChooseSaveLocationMenuItem(),
                                                                   new SaveOptionsMenuItem()  },
                                           new List<IMenu>()),
                new ReturnToMainMenu("Return to Main Menu")
            };
        }

        /// <summary>
        /// Display the current menu and any menu items in it
        /// </summary>
        public void DisplayMenu() {
            for (int i = 0; i < _menuList.Count; i++) {
                Console.WriteLine("{0} - {1}", (i + 1), _menuList[i].MenuTitle);
            }
        }

        /// <summary>
        /// Select the selected menu item in the menu
        /// </summary>
        /// <param name="menuSelected">Whichever item was selected</param>
        /// <returns>the menu item selected, otherwise null if invalid</returns>
        public IMenu SelectMenu(int menuSelected) {
            try {
                return _menuList[menuSelected - 1];
            } catch {
                return null; //Invalid menu item
            }
        }
    }
}
