﻿using System;
using System.Collections.Generic;

namespace Pork {
    class RegularMenu : IMenu {
        public string MenuTitle { private set; get; }

        private List<IMenuItem> _menuItems;
        private List<IMenu> _subMenus;

        /// <summary>
        /// Creates a menu that only has menu items in it, no submenus
        /// </summary>
        /// <param name="menuTitle">The title of the menu</param>
        public RegularMenu(string menuTitle, List<IMenuItem> menuItems, List<IMenu> subMenus) {
            MenuTitle = menuTitle;

            _menuItems = new List<IMenuItem>(menuItems);
            _subMenus = new List<IMenu>(subMenus);            
        }

        /// <summary>
        /// Display all the menu items in the menu
        /// </summary>
        public void DisplayMenu() {
            int menuNumber = 0;
            for (int i = 0; i < _menuItems.Count; i++, menuNumber++) {
                Console.Write(String.Format("{0} - ", (menuNumber + 1)));
                _menuItems[i].DrawMenuItem();
                Console.WriteLine();
            }

            for (int i = 0; i < _subMenus.Count; i++, menuNumber++) {
                Console.WriteLine(String.Format("{0} - {1}", (menuNumber + 1), _subMenus[i].MenuTitle));
            }
        }

        /// <summary>
        /// Selects the menu choice specified
        /// </summary>
        /// <param name="menuSelected">The ZERO-based selected item</param>
        /// <returns>Always returns null</returns>
        public IMenu SelectMenu(int menuSelected) {
            menuSelected--;

            //Must be in the list
            if (menuSelected >= _menuItems.Count + _subMenus.Count || menuSelected < 0) {
                return null;
            }

            try {
                if (menuSelected < _menuItems.Count) {
                    _menuItems[menuSelected].ActivateItem();
                } else {
                    _subMenus[menuSelected - _menuItems.Count].SelectMenu(menuSelected);
                }
            } catch (Exception err) {
                throw err;
            }

            return null;
        }
    }
}
