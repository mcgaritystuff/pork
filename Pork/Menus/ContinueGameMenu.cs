﻿namespace Pork {
    class ContinueGameMenu : IMenu {
        public string MenuTitle { private set; get; }

        /// <summary>
        /// Creates a continue game menu
        /// </summary>
        /// <param name="menuTitle">The title of the menu</param>
        public ContinueGameMenu(string menuTitle) {
            MenuTitle = menuTitle;
        }

        /// <summary>
        /// Unpause the game and set gamestate to GameState.Gameplay
        /// </summary>
        public void DisplayMenu() {
            GameVariables.CurrentGameState = GameState.Gameplay;
            GameVariables.GamePaused = false;
            GameVariables.RefreshScreen = true;
        }

        /// <summary>
        /// Does nothing
        /// </summary>
        /// <param name="menuSelected">N/A</param>
        /// <returns>Always returns null</returns>
        public IMenu SelectMenu(int menuSelected) {
            return null;
        }
    }
}
