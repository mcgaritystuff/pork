﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Pork {
    class ChooseAvatarMenuItem : IMenuItem {

        /// <summary>
        /// A list of all the avatar contents from files in Menus/ directory that start with "avatar"
        /// </summary>
        private List<string[]> _avatars = (new Func<List<string[]>>(() => {
            List<string[]> avatarList = new List<string[]>();
            List<string> files = Directory.GetFiles(GameVariables.AvatarsDirectory, "avatar*.txt").ToList();
            files.ForEach(file => avatarList.Add(File.ReadAllLines(file)));
            return avatarList;
        }))();

        private string[] _currentAvatar;

        /// <summary>
        /// Create a new choose avatar menu
        /// </summary>
        public ChooseAvatarMenuItem() {
            _currentAvatar = _avatars.FirstOrDefault();
            GameVariables.CurrentMenuPlayerDetails.Avatar = _currentAvatar;
        }

        /// <summary>
        /// Swap between avatars
        /// </summary>
        public void ActivateItem() {
            _currentAvatar = _avatars[(_avatars.IndexOf(_currentAvatar) + 1) % _avatars.Count];
            GameVariables.CurrentMenuPlayerDetails.Avatar = _currentAvatar;
        }

        /// <summary>
        /// Draw the menu avatar item!
        /// </summary>
        public void DrawMenuItem() {
            string str = "Appearance:";

            foreach (var line in _currentAvatar) {
                str += string.Format("\n\t{0}", line);
            }

            str.WriteToConsole();
        }
    }
}
