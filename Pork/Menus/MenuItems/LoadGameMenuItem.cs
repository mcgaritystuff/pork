﻿using System;

namespace Pork {
    class LoadGameMenuItem : IMenuItem {
        private int _slotNumber;

        /// <summary>
        /// Create a load game slot
        /// </summary>
        /// <param name="slotNumber">The slot number for the game data</param>
        public LoadGameMenuItem(int slotNumber) {
            _slotNumber = slotNumber;
        }

        /// <summary>
        /// Load the game selected
        /// </summary>
        public void ActivateItem() {
            GlobalMethods.LoadGameData(_slotNumber);
            "Game Loaded. Press Enter to continue.".WriteToConsole(ItemColors.GoodMessage);
            Console.ReadLine();
            //TODO: START GAME ON LOADED GAME
        }

        /// <summary>
        /// Draw the data for the game to load
        /// </summary>
        public void DrawMenuItem() {
            string.Format("\n\tSaved Game #{0}\n\tName: {1}\tAF: ${2}\n\tLevel: {3}\tGame Time: {4}\n", _slotNumber, "N/A", 0, 0, "0:00:00").WriteToConsole();
        }
    }
}
