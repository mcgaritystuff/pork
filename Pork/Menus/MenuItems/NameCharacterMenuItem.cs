﻿using System;

namespace Pork {
    class NameCharacterMenuItem : IMenuItem {
        private readonly int _maxCharLength = 10;

        /// <summary>
        /// Create an area to name the character
        /// </summary>
        public NameCharacterMenuItem() {
            GameVariables.CurrentMenuPlayerDetails.PlayerName = GameVariables.ProgramName;
        }

        /// <summary>
        /// Allow the player to change the character name
        /// </summary>
        public void ActivateItem() {
            Console.Write("Name your new character (max {0} char): ", _maxCharLength);
            string currName = Console.ReadLine();
            if (currName.Length <= _maxCharLength) {
                GameVariables.CurrentMenuPlayerDetails.PlayerName = currName;
            }
        }

        /// <summary>
        /// Draw the setting
        /// </summary>
        public void DrawMenuItem() {
            string.Format("Character Name: {0}", GameVariables.CurrentMenuPlayerDetails.PlayerName).WriteToConsole();
        }
    }
}
