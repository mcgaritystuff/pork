﻿namespace Pork {
    class ChooseFontSize : IMenuItem {
        // Change the font size to the console
        public void ActivateItem() {
            GameVariables.CurrentFont = ConsoleHelper.ConsoleFonts[(GameVariables.CurrentFont.Index + 1) % ConsoleHelper.ConsoleFontsCount];
            ConsoleHelper.SetConsoleFont(GameVariables.CurrentFont.Index);
        }

        // Draw the menu text for this item
        public void DrawMenuItem() {
            string.Format("Font Size: {0} x {1}", GameVariables.CurrentFont.SizeX, GameVariables.CurrentFont.SizeY).WriteToConsole();
        }
    }
}
