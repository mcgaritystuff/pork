﻿namespace Pork {
    class ToggleMusicMenuItem : IMenuItem {
        /// <summary>
        /// Toggle the game music on and off
        /// </summary>
        public void ActivateItem() {
            GameVariables.MusicEnabled = !GameVariables.MusicEnabled;
            if (!GameVariables.MusicEnabled) {
                GlobalMethods.StopMusic();
            } else {
                switch (GameVariables.CurrentGameState) {
                    case GameState.Gameplay:
                    case GameState.GameplayMenu:
                        GlobalMethods.PlayMusic(GameVariables.GameMusicFile);
                        break;
                    case GameState.MainMenu:
                        GlobalMethods.PlayMusic(GameVariables.MenuMusicFile);
                        break;
                }
            }
        }

        /// <summary>
        /// Draw the setting
        /// </summary>
        public void DrawMenuItem() {
            string.Format("[{0}] Music", GameVariables.MusicEnabled ? "*" : " ").WriteToConsole();
        }
    }
}
