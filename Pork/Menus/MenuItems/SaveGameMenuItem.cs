﻿using System;

namespace Pork {
    class SaveGameMenuItem : IMenuItem {
        private int _slotNumber;

        /// <summary>
        /// Create a save game slot
        /// </summary>
        /// <param name="slotNumber">The save slot to save the data to</param>
        public SaveGameMenuItem(int slotNumber) {
            _slotNumber = slotNumber;
        }

        /// <summary>
        /// Save the game data
        /// </summary>
        public void ActivateItem() {
            GlobalMethods.SaveGameData(_slotNumber);
            "Game Saved. Press Enter to continue.".WriteToConsole(ItemColors.GoodMessage);
            Console.ReadLine();
        }

        /// <summary>
        /// Draw the game data to save over
        /// </summary>
        public void DrawMenuItem() {
            string.Format("\n\tSaved Game #{0}\n\tName: {1}\tAF: ${2}\n\tLevel: {3}\tGame Time: {4}\n", _slotNumber, "N/A", 0, 0, "0:00:00").WriteToConsole();
        }
    }
}
