﻿namespace Pork {
    class SaveOptionsMenuItem : IMenuItem {
        /// <summary>
        /// Save game configuration
        /// </summary>
        public void ActivateItem() {
            GlobalMethods.SaveGameConfiguration();
        }

        /// <summary>
        /// Draw the setting
        /// </summary>
        public void DrawMenuItem() {
            "Save Settings".WriteToConsole(ItemColors.GoodMessage);
        }
    }
}
