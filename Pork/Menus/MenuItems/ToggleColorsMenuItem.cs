﻿namespace Pork {
    class ToggleColorsMenuItem : IMenuItem {
        /// <summary>
        /// Toggle colors on and off
        /// </summary>
        public void ActivateItem() {
            GameVariables.ColorsEnabled = !GameVariables.ColorsEnabled;
        }

        /// <summary>
        /// Show the setting
        /// </summary>
        public void DrawMenuItem() {
            string.Format("[{0}] Show Colors", GameVariables.ColorsEnabled ? "*" : " ").WriteToConsole();
        }
    }
}
