﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Pork {
    class ChoosePlayerColorMenuItem : IMenuItem {

        /// <summary>
        /// A list of all player colors defined
        /// </summary>
        private List<Color> _colors = new List<Color>() {
            ItemColors.PlayerLime,
            ItemColors.PlayerBlue,
            ItemColors.PlayerPink,
            ItemColors.PlayerYellow,
        };

        private Color _currentColor;

        /// <summary>
        /// Create the choose player menu and set a default color
        /// </summary>
        public ChoosePlayerColorMenuItem() {
            _currentColor = _colors.FirstOrDefault();
            GameVariables.CurrentMenuPlayerDetails.PlayerColor = _currentColor;
        }

        /// <summary>
        /// Change the player color 
        /// </summary>
        public void ActivateItem() {
            _currentColor = _colors[(_colors.IndexOf(_currentColor) + 1) % _colors.Count];
            GameVariables.CurrentMenuPlayerDetails.PlayerColor = _currentColor;
        }

        /// <summary>
        /// Draw the selection item
        /// </summary>
        public void DrawMenuItem() {
            "Player Color: ".WriteToConsole();
            string.Format("{0}", _currentColor.Name.SplitByCapitalLetters()).WriteToConsole(_currentColor);
        }
    }
}
