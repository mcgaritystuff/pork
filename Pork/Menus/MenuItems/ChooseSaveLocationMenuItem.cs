﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Pork {
    class ChooseSaveLocationMenuItem : IMenuItem {
        /// <summary>
        /// Create the menu item to save a location. 
        /// </summary>
        public ChooseSaveLocationMenuItem() {
            CreateSaveDirectory(GameVariables.SaveGameLocation, GameVariables.SaveGameLocation);
        }

        /// <summary>
        /// Allow the player to type up a new location to save stuff to
        /// </summary>
        public void ActivateItem() {
            List<char> buffer = new List<char>(GameVariables.SaveGameLocation.ToCharArray().ToList());
            string.Format("Specify the new location:\n{0}", GameVariables.SaveGameLocation).WriteToConsole();

            // Let the player type out the new location and when they're done, they push enter
            ConsoleKeyInfo keyInfo = Console.ReadKey(true);
            while (keyInfo.Key != ConsoleKey.Enter) {
                switch (keyInfo.Key) {
                    case ConsoleKey.Backspace:
                        if (buffer.Count > 0) {
                            buffer.RemoveAt(buffer.Count - 1);
                            if (Console.CursorLeft > 0) {
                                Console.Write("\b \b");
                            } else {
                                Console.SetCursorPosition(Console.BufferWidth - 1, Console.CursorTop - 1);
                                Console.Write(" \b");
                                Console.SetCursorPosition(Console.BufferWidth - 1, Console.CursorTop - 1);
                            }
                        }
                        break;
                    default:
                        buffer.Add(keyInfo.KeyChar);
                        Console.Write(keyInfo.KeyChar);
                        break;
                }
                keyInfo = Console.ReadKey(true);
            }

            // Now that the player's selected the location, set the text to that location
            string currLoc = "";
            foreach (var item in buffer) {
                currLoc += item;
            }

            // Create the directory and set the variable
            if (CreateSaveDirectory(GameVariables.SaveGameLocation, currLoc)) {
                GameVariables.SaveGameLocation = currLoc;
            }
        }

        /// <summary>
        /// Draw the setting
        /// </summary>
        public void DrawMenuItem() {
            string.Format("Save Game Location:\n{0}", GameVariables.SaveGameLocation).WriteToConsole();
        }

        /// <summary>
        /// Create a save directory and write the settings contents to it
        /// </summary>
        /// <param name="prevLocation">If this is being moved, move the file from a previous location</param>
        /// <param name="location">The new location to save to</param>
        /// <returns></returns>
        private bool CreateSaveDirectory(string prevLocation, string location) {
            try {
                if (!string.IsNullOrEmpty(location)) {
                    System.Text.RegularExpressions.Regex checkPathRegex = new System.Text.RegularExpressions.Regex(@"^\w:\\.*");
                    if (checkPathRegex.Matches(location).Count == 0) {
                        throw new Exception("Must have drive letter to start with!");
                    }

                    if (!string.IsNullOrEmpty(prevLocation) && !System.IO.Directory.Exists(prevLocation) && !System.IO.Directory.Exists(location)) {
                        System.IO.Directory.CreateDirectory(location);
                    } else if (!prevLocation.Equals(location)) {
                        if (System.IO.Directory.Exists(location)) {
                            string[] files = System.IO.Directory.GetFiles(prevLocation);
                            string fileName, destFile, sourceFile;

                            foreach (string s in files) {
                                fileName = System.IO.Path.GetFileName(s);
                                destFile = System.IO.Path.Combine(location, fileName);
                                sourceFile = System.IO.Path.Combine(prevLocation, fileName);
                                System.IO.File.Copy(s, destFile, true);
                                System.IO.File.Delete(sourceFile);
                            }
                        } else {
                            System.IO.Directory.Move(prevLocation, location);
                        }
                    }
                } else {
                    throw new Exception("Must have drive letter to start with (e.g. d:\\)!");
                }
            } catch (Exception error) {
                Console.WriteLine("\nError creating save directory: {0}\nPress Enter to continue.", error.Message);
                Console.ReadLine();
                return false;
            }

            return true;
        }
    }
}
