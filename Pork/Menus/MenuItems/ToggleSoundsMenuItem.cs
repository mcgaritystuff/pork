﻿namespace Pork {
    class ToggleSoundsMenuItem : IMenuItem {
        /// <summary>
        /// Toggle sounds on and off
        /// </summary>
        public void ActivateItem() {
            GameVariables.SoundEnabled = !GameVariables.SoundEnabled;
        }

        /// <summary>
        /// Draw the setting
        /// </summary>
        public void DrawMenuItem() {
            string.Format("[{0}] Sounds", GameVariables.SoundEnabled ? "*" : " ").WriteToConsole();
        }
    }
}
