﻿using System;
using System.Collections.Generic;

namespace Pork {
    class MainMenu : IMenu {
        public string MenuTitle { private set; get; }

        private List<IMenu> _menuList;

        /// <summary>
        /// The main menu
        /// </summary>
        public MainMenu() {
            MenuTitle = "Main Menu";
            _menuList = new List<IMenu>() {
                //Add menu items in here. Example: new RegularMenu(menuTitle, items). Make sure all the menus implement IMenu
                //It might be better to pass in a menu list instead of doing it here since we can have different types of
                //main menus.
                new RegularMenu("New Game", new List<IMenuItem>() { new NameCharacterMenuItem(), new ChooseAvatarMenuItem(), new ChoosePlayerColorMenuItem() },
                                            new List<IMenu>() { new StartGameMenu("Start Game") }),
                new RegularMenu("Load", new List<IMenuItem>() { new LoadGameMenuItem(0), new LoadGameMenuItem(1), new LoadGameMenuItem(2) }, new List<IMenu>()),
                new StartTutorialMenu("Tutorial"),
                new RegularMenu("Options", new List<IMenuItem>() { new ToggleSoundsMenuItem(), new ToggleMusicMenuItem(),
                                                                   new ToggleColorsMenuItem(), new ChooseFontSize(),
                                                                   new ChooseSaveLocationMenuItem(), new SaveOptionsMenuItem() },
                                           new List<IMenu>()),
                new ExitMenu("Exit Game"),
            };
        }

        /// <summary>
        /// Display the current menu and any menu items in it
        /// </summary>
        public void DisplayMenu() {
            for (int i = 0; i < _menuList.Count; i++) {
                Console.WriteLine("{0} - {1}", (i + 1), _menuList[i].MenuTitle);
            }
        }

        /// <summary>
        /// Select the selected menu item in the menu
        /// </summary>
        /// <param name="menuSelected">Whichever item was selected</param>
        /// <returns>the menu item selected, otherwise null if invalid</returns>
        public IMenu SelectMenu(int menuSelected) {
            try {
                return _menuList[menuSelected - 1];
            } catch {
                return null; //Invalid menu item
            }
        }
    }
}
