﻿namespace Pork {
    class ReturnToMainMenu : IMenu {
        public string MenuTitle { private set; get; }

        /// <summary>
        /// Create the return to main menu, menu
        /// </summary>
        /// <param name="menuTitle">The title of the menu</param>
        public ReturnToMainMenu(string menuTitle) {
            MenuTitle = menuTitle;
        }

        /// <summary>
        /// Refreshes/clears the game, sets to GameState.MainMenu, and plays music if applicable.
        /// </summary>
        public void DisplayMenu() {
            GameVariables.GamePaused = false;
            GameVariables.RefreshScreen = true;
            GameVariables.CurrentGameState = GameState.MainMenu;
            GlobalMethods.PlayMusic(GameVariables.MenuMusicFile);
            GlobalMethods.ClearGameBuffer();
        }

        /// <summary>
        /// Does nothing
        /// </summary>
        /// <param name="menuSelected">N/A</param>
        /// <returns>Always returns null</returns>
        public IMenu SelectMenu(int menuSelected) {
            return null;
        }
    }
}
