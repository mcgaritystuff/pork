﻿using System;

namespace Pork {
    class StartGameMenu : IMenu {
        public string MenuTitle { private set; get; }

        /// <summary>
        /// Creates a start game menu
        /// </summary>
        /// <param name="menuTitle">The title of the menu</param>
        public StartGameMenu(string menuTitle) {
            MenuTitle = menuTitle;
        }

        /// <summary>
        /// Not used at all
        /// </summary>
        public void DisplayMenu() { }

        /// <summary>
        /// Clears the console and starts the game!
        /// </summary>
        /// <param name="menuSelected">Not needed, ignored</param>
        /// <returns>Always returns null</returns>
        public IMenu SelectMenu(int menuSelected) {
            Console.Clear();
            GameVariables.CurrentGameState = GameState.Gameplay;
            GameVariables.InitializeGameSettings();
            return null;
        }
    }
}
